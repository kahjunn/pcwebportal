import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ScrollToTop from './modules/Home/components/ScrollToTop';
import Header from './modules/Home/components/Header';
import Footer from './modules/Home/components/Footer';
import HomePage from './modules/Home/components/HomePage';
import BuildAPCPage from './modules/BuildAPC/components/BuildAPCPage';
import QuotationPage from './modules/BuildAPC/components/QuotationPage';
import CategoryPage from './modules/BrowsePCParts/components/CategoryPage';
import CPU from './modules/BrowsePCParts/components/CPU';
import GPU from './modules/BrowsePCParts/components/GPU';
import MOBO from './modules/BrowsePCParts/components/Motherboard';
import RAM from './modules/BrowsePCParts/components/RAM';
import STORAGE from './modules/BrowsePCParts/components/Storage';
import POWER from './modules/BrowsePCParts/components/Power';
import COOLER from './modules/BrowsePCParts/components/Cooler';
import CASE from './modules/BrowsePCParts/components/Casing';
import CPUDetailPage from './modules/BrowsePCParts/components/CPUDetailPage';
import GPUDetailPage from './modules/BrowsePCParts/components/GPUDetailPage';
import MOBODetailPage from './modules/BrowsePCParts/components/MotherboardDetailPage';
import RAMDetailPage from './modules/BrowsePCParts/components/RAMDetailPage';
import StorageDetailPage from './modules/BrowsePCParts/components/StorageDetailPage';
import PowerDetailPage from './modules/BrowsePCParts/components/PowerDetailPage';
import CoolerDetailPage from './modules/BrowsePCParts/components/CoolerDetailPage';
import CASEDetailPage from './modules/BrowsePCParts/components/CasingDetailPage';
import PCBuildsPage from './modules/BrowsePCBuilds/components/PCBuildsPage';
import PCBuildDetailPage from './modules/BrowsePCBuilds/components/PCBuildDetailPage';
import PromotionPage from './modules/Promotions/PromotionPage';
import AccountDetailsPage from './modules/User/components/AccountDetailsPage';
import OrderPage from './modules/User/components/OrderPage';
import OrderDetailsPage from './modules/User/components/OrderDetailsPage';
import CPUCompare from './modules/BrowsePCParts/components/CPUComparePage';
import GPUCompare from './modules/BrowsePCParts/components/GPUComparePage';
import MOBOCompare from './modules/BrowsePCParts/components/MotherboardComparePage';
import RAMCompare from './modules/BrowsePCParts/components/RAMComparePage';
import StorageCompare from './modules/BrowsePCParts/components/StorageComparePage';
import PowerCompare from './modules/BrowsePCParts/components/PowerComparePage';
import CoolerCompare from './modules/BrowsePCParts/components/CoolerComparePage';
import CASECompare from './modules/BrowsePCParts/components/CasingComparePage';
import BuildCompare from './modules/BrowsePCBuilds/components/PCBuildComparePage';
import './App.css';

class App extends Component { 
  render() {
    return ( 
      <div className="App">
        <div className="image">
        <Router>
          <ScrollToTop> 
          <Header />
          <Route exact path="/" component={HomePage} />
          <Route exact path="/buildapc" component={BuildAPCPage} /> 
          <Route exact path="/buildapc/quotation/" component={QuotationPage} />
          
          <Route exact path="/pc/category" component={CategoryPage} />
          <Route exact path="/pc/cpu" component={CPU} /> 
          <Route exact path="/pc/cpu/details/:id" component={CPUDetailPage} />

          <Route exact path="/pc/cpu/compare" component={CPUCompare} />
          <Route exact path="/pc/gpu" component={GPU} />
          <Route exact path="/pc/gpu/details/:id" component={GPUDetailPage} />
          <Route exact path="/pc/gpu/compare" component={GPUCompare} />

          <Route exact path="/pc/motherboard" component={MOBO} />
          <Route exact path="/pc/motherboard/details/:id" component={MOBODetailPage} />
          <Route exact path="/pc/motherboard/compare" component={MOBOCompare} />

          <Route exact path="/pc/ram" component={RAM} />
          <Route exact path="/pc/ram/details/:id" component={RAMDetailPage} />
          <Route exact path="/pc/ram/compare" component={RAMCompare} />

          <Route exact path="/pc/storage" component={STORAGE} />
          <Route exact path="/pc/storage/details/:id" component={StorageDetailPage} />
          <Route exact path="/pc/storage/compare" component={StorageCompare} />

          <Route exact path="/pc/power-supply" component={POWER} />
          <Route exact path="/pc/power-supply/details/:id" component={PowerDetailPage} />
          <Route exact path="/pc/power-supply/compare" component={PowerCompare} />

          <Route exact path="/pc/cooler" component={COOLER} />
          <Route exact path="/pc/cooler/details/:id" component={CoolerDetailPage} />
          <Route exact path="/pc/cooler/compare" component={CoolerCompare} />

          <Route exact path="/pc/case" component={CASE} />
          <Route exact path="/pc/case/details/:id" component={CASEDetailPage} />
          <Route exact path="/pc/case/compare" component={CASECompare} />

          <Route exact path="/pcbuilds/compare" component={BuildCompare} />
          <Route exact path="/pcbuilds" component={PCBuildsPage} />
          <Route exact path="/pcbuilds/details/:id" component={PCBuildDetailPage} /> 

          <Route exact path="/promotion" component={PromotionPage} />
          <Route exact path="/user/account" component={AccountDetailsPage} />
          <Route exact path="/user/orders" component={OrderPage} /> 
          <Route exact path="/user/orders/details/:id" component={OrderDetailsPage} />
          <Footer />
          </ScrollToTop>
        </Router>
        </div>
      </div>
    )
  }
}

export default App;
