import React, { Component } from 'react';
import { Row, Col, Column, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/background.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/ComparePCParts.scss';
import PC from '../../../assets/build.png';

const { Group, Label, Control } = Form;

class PCBuildComparePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pcbuildItems1: [],
      pcbuildItems2: [],
      pcbuild1: {},
      pcbuild2: {},
    };
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleSubmit2 = this.handleSubmit2.bind(this);
  }

  componentDidMount() {
    this.getPCBuild();
  }

  async getPCBuild() {
    const res = await apiCaller.get('/api/pcbuild/');
    const pcbuildItems1 = res.data.payload;
    const pcbuildItems2 = res.data.payload;
    this.setState({ pcbuildItems1, pcbuildItems2 });
    console.log('Items1: ', pcbuildItems1);
    console.log('Items2: ', pcbuildItems2);
  }

  async handleSubmit1(event) {
    try{
      console.log('Value: ', event.target.value);
      const pcbuildId1 = event.target.value;
      const res = await apiCaller(`api/pcbuild/${pcbuildId1}`);
      const pcbuild1 = res.data.payload;
      this.setState({ pcbuild1 });
      // console.log('pcbuild returned: ', pcbuild1);
    } catch (e) {
      console.log(e);
    }
  }

  async handleSubmit2(event) {
    try{
      console.log('Value: ', event.target.value);
      const pcbuildId2 = event.target.value;
      const res = await apiCaller(`api/pcbuild/${pcbuildId2}`);
      const pcbuild2 = res.data.payload;
      this.setState({ pcbuild2 });
      console.log('pcbuild returned 2: ', pcbuild2);
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { pcbuildItems1, pcbuildItems2, pcbuild1, pcbuild2 } = this.state;
    console.log('pcbuild to be assigned: ', pcbuild1);
    console.log('pcbuild to be assigned 2: ', pcbuild2);
    return (
      <div>
        <div className="banner">
          <h1>Compare PC Build</h1>
        </div>

        <div className="pcbuildcompare-container">
          <div>
            <Row md={4}> 
              <Col md={{offset: 4}} className="pcbuild-column-1">
                <div className="mt-4">
                  <Group>
                    <Label>PC Build 1: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit1}
                    >
                      <option selected disabled>Choose...</option>
                      {pcbuildItems1.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-image" src={PC} />
                    <h5 className="parts-compare-price">RM {pcbuild1.price}</h5>
                  </div>
                </div>
              </Col>
              <Col md={{ offset: 1}}>
              <div className="mt-4">
                  <Group>
                    <Label>PC Build 2: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit2}
                    >
                      <option selected disabled>Choose...</option>
                      {pcbuildItems2.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-compare-image" src={PC} />
                    <h5 className="parts-compare-price">RM {pcbuild2.price}</h5>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
            <div className="parts-compare-table">
              <table>
                <tr>
                  <th width="15%">PC Build Name</th>
                  <td width="20% "className="parts-compare-cell-1">{pcbuild1.name}</td>
                  <td width="20%" className="parts-compare-cell-2">{pcbuild2.name}</td>
                </tr>
                <tr>
                  <th>CPU</th>
                  <td className="parts-compare-cell-1">{pcbuild1.cpu}</td>
                  <td className="parts-compare-cell-2">{pcbuild2.cpu}</td>
                </tr>
                <tr>
                  <th>Motherboard</th>
                    <td className="parts-compare-cell-1">{pcbuild1.mobo}</td>
                  <td className="parts-compare-cell-2">{pcbuild2.mobo}</td>
                </tr>
                <tr>
                  <th>Memory</th>
                  <td className="parts-compare-cell-1">{pcbuild1.memory}</td>
                  <td className="parts-compare-cell-2">{pcbuild2.memory}</td>
                </tr>
                <tr>
                  <th>Storage</th>
                  <td className="parts-compare-cell-1">{pcbuild1.storage}</td>
                  <td className="parts-compare-cell-2">{pcbuild2.storage}</td>
                </tr>
                <tr>
                  <th>GPU</th>
                  <td className="parts-compare-cell-1">{pcbuild1.gpu}</td>
                  <td className="parts-compare-cell-2">{pcbuild2.gpu}</td>
                </tr>
                <tr>
                  <th>PC Case</th>
                  <td className="parts-compare-cell-1">{pcbuild1.casing}</td>
                  <td className="parts-compare-cell-2">{pcbuild2.casing}</td>
                </tr>
                <tr>
                  <th>Power Supply</th>
                  <td className="parts-compare-cell-1">{pcbuild1.psu}</td>
                  <td className="parts-compare-cell-2">{pcbuild2.psu}</td>
                </tr>
                <tr>
                  <th>OS</th>
                  <td className="parts-compare-cell-1">{pcbuild1.os}</td>
                  <td className="parts-compare-cell-2">{pcbuild2.os}</td>
                </tr>
                <tr>
                  <th>CPU Cooler</th>
                  <td className="parts-compare-cell-1">{pcbuild1.cooler}</td>
                  <td className="parts-compare-cell-2">{pcbuild2.cooler}</td>
                </tr>
              </table> 
            </div>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default PCBuildComparePage;