import React, { Component } from 'react';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/background.jpg';
import '../styles/PCBuildDetailPage.scss';
import apiCaller from '../../../tools/apiUtil';
import PC from '../../../assets/build.png';

class PCBuildDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buildId: props.match.params.id,
      build: {} 
    };
  }

  componentDidMount() {
    this.getBuild();
  }

  async getBuild() {
    const res = await apiCaller.get(`/api/pcbuild/${this.state.buildId}`);
    const build = res.data.payload;
    this.setState({ build });
  }

  render() {
    const { build } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>PC Build Details</h1>
        </div>

        <div className="pcbuild-container">
          <div className="detail-column">
            <row>
              <div className="build-preview">
                <div className="preview-a">
                  <h5 className="build-name">{build.name}</h5>
                  <h5 className="build-price">RM {build.price}</h5>
                </div>
                <div className="preview-b">
                  <img className="build-image" src={PC} />
                </div>
              </div>
            </row>

            <row>
            <div className="build-table">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <th width="25%">CPU</th>
                  <td>{build.cpu}</td>
                </tr>
                <tr>
                  <th>Motherboard</th>
                  <td>{build.mobo}</td>
                </tr>
                <tr>
                  <th>Memory</th>
                  <td>{build.memory}</td>
                </tr>
                <tr>
                  <th>Storage</th>
                  <td>{build.storage}</td>
                </tr>
                <tr>
                  <th>GPU</th>
                  <td>{build.gpu}</td>
                </tr>
                <tr>
                  <th>PC Case</th>
                  <td>{build.casing}</td>
                </tr>
                <tr>
                  <th>Power Supply</th>
                  <td>{build.psu}</td>
                </tr>
                <tr>
                  <th>OS</th>
                  <td>{build.os}</td>
                </tr>
                <tr>
                  <th>CPU Cooler</th>
                  <td>{build.cooler}</td>
                </tr>
              </table> 
            </div>
            </row>
          </div>
        </div>
      </div>
    )
  }
}

export default PCBuildDetailPage;