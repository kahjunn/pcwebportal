import React, { Component } from 'react'; 
import apiCaller from '../../../tools/apiUtil';
import { Link } from 'react-router-dom';
import '../../../styles/Banner.scss';
import '../../../styles/CompareButton.scss';
import '../styles/PCBuildsPage.scss';
import TEST from '../../../assets/background.jpg';
import PC from '../../../assets/build.png';

class PCBuildsPage extends Component { 
  constructor(props) { 
    super(props);
    this.state = {
      builds: []
    };
  }

  componentDidMount() {
    this.getBuilds();
  }

  async getBuilds() {
    const res = await apiCaller.get('/api/pcbuild');
    const builds = res.data.payload;
    this.setState({ builds }); 
    console.log('Items: ', builds);
  }

  render() { 
    const { builds } = this.state;
    return ( 
      <div> 
        <div className="banner">
          <h1>Browse PC Builds</h1>
        </div>
        <row>
        <div className="pcbuilds-container">
          <div className="compare-container">
            <Link to="/pcbuilds/compare">
              <button className="compare-button"><span>Compare </span></button>
            </Link>
          </div>

          {builds.map((data) => 
            <div className="card-column">
              <div className="card">
                <img className="card-image" src={PC} />
                <div className="card-content">
                  <h1 className="card-title">{data.name}</h1>
                  <p className="card-detail">{data.description}</p>
                  <div className="card-price">
                    <div className="a">
                      <h5>Price: </h5>
                      <h5 className="card-pricing">RM {data.price}</h5>
                    </div>
                    <div className="b">
                      <Link to={`/pcbuilds/details/${data.id}`}>
                        <button className="card-button"><span>View </span></button>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
        </row>
      </div>
    );
  }
}

export default PCBuildsPage;


