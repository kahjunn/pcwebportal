import React, { Component } from 'react';
import Typy from 'typy';
import { Link } from 'react-router-dom';
import '../styles/CPU.scss';
import '../../../styles/PCPartsTable.scss';
import CPUOther from './CPUOther.js';
import apiCaller from '../../../tools/apiUtil';

class CPU extends Component{ 
  constructor(props) { 
    super(props);
    this.state = {
      cpuItems: []
    };
  }

  componentDidMount() {
    this.getAllCPU();
  }

  async getAllCPU() {
    const res = await apiCaller.get('/api/cpu/');
    const cpuItems = res.data.payload;
    this.setState({ cpuItems });
  }
  
  render() { 
    const { cpuItems } = this.state;
    console.log("CPU", cpuItems);
    return (
      <div>
        <div className="banner">
          <h1>Computer Processing Unit (CPU)</h1>
        </div>
        <div className="parts-container">
          <CPUOther />
          <div className="parts-table">
            <table>
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th width="20%">Name</th>
                  <th width="10%">Brand</th>
                  <th width="10%">Core Count</th>
                  <th width="10%">Clock Speed</th>
                  <th width="10%" className="price">Price</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              {cpuItems.map((data) => 
              <tbody>
                <tr>            
                  <td width="5%">{data.id}</td>
                  <td width="20%">{data.name}</td>
                  <td width="10%">{data.brand}</td>
                  <td width="10%">{data.coreCount}</td>
                  <td width="10%">{data.clockSpeed}</td>
                  <td width="10%" className="price">RM {data.price}</td> 
                  <td width="10%">
                    <Link to={`cpu/details/${data.id}`}>
                      <button className="view-button"><span>View </span></button> 
                    </Link>
                  </td> 
                </tr>
              </tbody>
              )}
            </table>  
          </div>
        </div>
      </div>
    );
  }
}

export default CPU;