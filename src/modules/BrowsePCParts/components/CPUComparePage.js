import React, { Component } from 'react';
import { Row, Col, Column, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/background.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/ComparePCParts.scss';
import INTEL from '../../../assets/intel.jpg';
import AMD from '../../../assets/amd.jpg';

const { Group, Label, Control } = Form;

class CPUComparePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cpuItems1: [],
      cpuItems2: [],
      cpu1: {},
      cpu2: {},
    };
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleSubmit2 = this.handleSubmit2.bind(this);
  }

  componentDidMount() {
    this.getCPU();
  }

  async getCPU() {
    const res = await apiCaller.get('/api/cpu/');
    const cpuItems1 = res.data.payload;
    const cpuItems2 = res.data.payload;
    this.setState({ cpuItems1, cpuItems2 });
    console.log('Items1: ', cpuItems1);
    console.log('Items2: ', cpuItems2);
  }

  async handleSubmit1(event) {
    try{
      console.log('Value: ', event.target.value);
      const cpuId1 = event.target.value;
      const res = await apiCaller(`api/cpu/${cpuId1}`);
      const cpu1 = res.data.payload;
      this.setState({ cpu1 });
      // console.log('CPU returned: ', cpu1);
    } catch (e) {
      console.log(e);
    }
  }

  async handleSubmit2(event) {
    try{
      console.log('Value: ', event.target.value);
      const cpuId2 = event.target.value;
      const res = await apiCaller(`api/cpu/${cpuId2}`);
      const cpu2 = res.data.payload;
      this.setState({ cpu2 });
      console.log('CPU returned 2: ', cpu2);
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { cpuItems1, cpuItems2, cpu1, cpu2 } = this.state;
    console.log('CPU to be assigned: ', cpu1);
    console.log('CPU to be assigned 2: ', cpu2);
    return (
      <div>
        <div className="banner">
          <h1>Compare CPU</h1>
        </div>

        <div className="cpucompare-container">
          <div>
            <Row md={4}> 
              <Col md={{offset: 4}} className="cpu-column-1">
                <div className="mt-4">
                  <Group>
                    <Label>CPU 1: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit1}
                    >
                      <option selected disabled>Choose...</option>
                      {cpuItems1.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-image" src={INTEL} />
                    <h5 className="parts-compare-price">RM {cpu1.price}</h5>
                  </div>
                </div>
              </Col>
              <Col md={{ offset: 1}}>
              <div className="mt-4">
                  <Group>
                    <Label>CPU 2: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit2}
                    >
                      <option selected disabled>Choose...</option>
                      {cpuItems2.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-compare-image" src={AMD} />
                    <h5 className="parts-compare-price">RM {cpu2.price}</h5>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
            <div className="parts-compare-table">
              <table>
                <tr>
                  <th width="15%">Name</th>
                  <td width="20% "className="parts-compare-cell-1">{cpu1.name}</td>
                  <td width="20%" className="parts-compare-cell-2">{cpu2.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-compare-cell-1">{cpu1.brand}</td>
                  <td className="parts-compare-cell-2">{cpu2.brand}</td>
                </tr>
                <tr>
                  <th>Model</th>
                  <td className="parts-compare-cell-1">{cpu1.model}</td>
                  <td className="parts-compare-cell-2">{cpu2.model}</td>
                </tr>
                <tr>
                  <th>Core Count</th>
                  <td className="parts-compare-cell-1">{ !cpu1.coreCount ? '' : `${cpu1.coreCount}`}</td>
                  <td className="parts-compare-cell-2">{ !cpu2.coreCount ? '' : `${cpu2.coreCount}`}</td>
                </tr>
                <tr>
                  <th>Clock Speed</th>
                  <td className="parts-compare-cell-1">{ !cpu1.clockSpeed ? '' : `${cpu1.clockSpeed} GHz`}</td>
                  <td className="parts-compare-cell-2">{ !cpu2.clockSpeed ? '' : `${cpu2.clockSpeed} GHz`}</td>
                </tr>
                <tr>
                  <th>Boost Clock Speed</th>
                  <td className="parts-compare-cell-1">{ !cpu1.boostClock ? '' : `${cpu1.boostClock} GHz`}</td>
                  <td className="parts-compare-cell-2">{ !cpu2.boostClock ? '' : `${cpu2.boostClock} GHz`}</td>
                </tr>
                <tr>
                  <th>Supported Socket</th>
                  <td className="parts-compare-cell-1">{cpu1.supportedSocket}</td>
                  <td className="parts-compare-cell-2">{cpu2.supportedSocket}</td>
                </tr>
                <tr>
                  <th>Threads</th>
                  <td className="parts-compare-cell-1">{ !cpu1.threads ? '' : `${cpu1.threads} Threads`}</td>
                  <td className="parts-compare-cell-2">{ !cpu2.threads ? '' : `${cpu2.threads} Threads`}</td>
                </tr>
                <tr>
                  <th>Core-Family</th>
                  <td className="parts-compare-cell-1">{cpu1.coreFamily}</td>
                  <td className="parts-compare-cell-2">{cpu2.coreFamily}</td>
                </tr>
                <tr>
                  <th>Cache Size</th>
                  <td className="parts-compare-cell-1">{!cpu1.cache ? '' : `${cpu1.cache} MB`} </td>
                  <td className="parts-compare-cell-2">{!cpu2.cache ? '' : `${cpu2.cache} MB`} </td>
                </tr>
                <tr>
                  <th>Maximum Supported Memory</th>
                  <td className="parts-compare-cell-1">{!cpu1.maxMemory ? '' : `${cpu1.maxMemory} GB`}</td>
                  <td className="parts-compare-cell-2">{!cpu2.maxMemory ? '' : `${cpu2.maxMemory} GB`}</td>
                </tr>
                <tr>
                  <th>Lithography</th>
                  <td className="parts-compare-cell-1">{ !cpu1.lithography ? '' : `${cpu1.lithography} nm`}</td>
                  <td className="parts-compare-cell-2">{ !cpu2.lithography ? '' : `${cpu2.lithography} nm`}</td>
                </tr>
              </table> 
            </div>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default CPUComparePage;