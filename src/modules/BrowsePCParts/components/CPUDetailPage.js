import React, { Component } from 'react';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/background.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/PCPartsDetail.scss';
import DUMMY2 from '../../../assets/intel.jpg';

class CPUDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cpuId: props.match.params.id,
      cpu: {} 
    };
  }

  componentDidMount() {
    this.getCPU();
  }

  async getCPU() {
    const res = await apiCaller.get(`/api/cpu/${this.state.cpuId}`);
    const cpu = res.data.payload;
    this.setState({ cpu });
  }

  render() {
    const { cpu } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>Computer Processing Unit (CPU) Details</h1>
        </div>

        <div className="cpuparts-detail-container">
          <div className="detail-column">
            <row>
              <div className="parts-detail-preview">
                <div className="parts-preview-a">
                  <h5 className="parts-price">RM {cpu.price}</h5>
                </div>
                <div className="parts-preview-b">
                  <img className="parts-image" src={DUMMY2} />
                </div>
              </div>
            </row>

            <row>
            <div className="parts-detail-table">
              <table>
                <tr>
                  <th width="38%">Name</th>
                  <td className="parts-table-name">{cpu.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-table-cell">{cpu.brand}</td>
                </tr>
                <tr>
                  <th>Core Count</th>
                  <td className="parts-table-cell">{cpu.coreCount} Cores</td>
                </tr>
                <tr>
                  <th>Clock Speed</th>
                  <td className="parts-table-cell">{cpu.clockSpeed} Ghz</td>
                </tr>
                <tr>
                  <th>Boost Clock Speed</th>
                  <td className="parts-table-cell">{cpu.boostClock} Ghz</td>
                </tr>
                <tr>
                  <th>Supported Socket</th>
                  <td className="parts-table-cell">{cpu.supportedSocket}</td>
                </tr>
                <tr>
                  <th>Threads</th>
                  <td className="parts-table-cell">{cpu.threads} Threads</td>
                </tr>
                <tr>
                  <th>Core-Family</th>
                  <td className="parts-table-cell">{cpu.coreFamily}</td>
                </tr>
                <tr>
                  <th>Cache Size</th>
                  <td className="parts-table-cell">{cpu.cache} MB</td>
                </tr>
                <tr>
                  <th>Maximum Supported Memory</th>
                  <td className="parts-table-cell">{cpu.maxMemory} GB</td>
                </tr>
                <tr>
                  <th>Lithography</th>
                  <td className="parts-table-cell">{cpu.lithography} nm</td>
                </tr>
              </table> 
            </div>
            </row>
          </div>
        </div>
      </div>
    )
  }
}

export default CPUDetailPage;