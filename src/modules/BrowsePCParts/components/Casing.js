import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../styles/PCPartsTable.scss';
import CasingOther from './CasingOther.js';
import apiCaller from '../../../tools/apiUtil';

class Casing extends Component{ 
  constructor(props) { 
    super(props);
    this.state = {
      caseItems: []
    };
  }

  componentDidMount() {
    this.getAllCase();
  }

  async getAllCase() {
    const res = await apiCaller.get('/api/case/');
    const caseItems = res.data.payload;
    this.setState({ caseItems });
  }

  render() { 
    const { caseItems } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>PC Case</h1>
        </div>
        <div className="parts-container">        
          <CasingOther />

          <div className="parts-table">
            <table>
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th width="20%">Name</th>
                  <th width="10%">Brand</th>
                  <th width="10%">Form Factor</th>
                  <th width="10%">Color</th>
                  <th width="10%" className="price">Price</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              {caseItems.map((data) => 
              <tbody>
                <tr>            
                  <td>{data.id}</td>
                  <td>{data.name}</td>
                  <td>{data.brand}</td>
                  <td>{data.formFactor}</td>
                  <td>{data.color}</td>
                  <td className="price">RM {data.price}</td> 
                  <td>
                    <Link to={`case/details/${data.id}`}>
                      <button className="view-button"><span>View </span></button>
                    </Link>
                  </td> 
                </tr>
              </tbody>
              )}
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default Casing;