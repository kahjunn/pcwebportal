import React, { Component } from 'react';
import { Row, Col, Column, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import '../../../styles/Banner.scss';
// import TEST from '../../../assets/background.jpg';
import TEST from '../../../assets/nzxt.jpg'
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/ComparePCParts.scss';

const { Group, Label, Control } = Form;

class CasingComparePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      casingItems1: [],
      casingItems2: [],
      casing1: {},
      casing2: {},
    };
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleSubmit2 = this.handleSubmit2.bind(this);
  }

  componentDidMount() {
    this.getCasing();
  }

  async getCasing() {
    const res = await apiCaller.get('/api/case/');
    const casingItems1 = res.data.payload;
    const casingItems2 = res.data.payload;
    this.setState({ casingItems1, casingItems2 });
    console.log('Items1: ', casingItems1);
    console.log('Items2: ', casingItems2);
  }

  async handleSubmit1(event) {
    try{
      console.log('Value: ', event.target.value);
      const casingId1 = event.target.value;
      const res = await apiCaller(`api/case/${casingId1}`);
      const casing1 = res.data.payload;
      this.setState({ casing1 });
      // console.log('casing returned: ', casing1);
    } catch (e) {
      console.log(e);
    }
  }

  async handleSubmit2(event) {
    try{
      console.log('Value: ', event.target.value);
      const casingId2 = event.target.value;
      const res = await apiCaller(`api/case/${casingId2}`);
      const casing2 = res.data.payload;
      this.setState({ casing2 });
      console.log('casing returned 2: ', casing2);
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { casingItems1, casingItems2, casing1, casing2 } = this.state;
    console.log('casing to be assigned: ', casing1);
    console.log('casing to be assigned 2: ', casing2);
    return (
      <div>
        <div className="banner">
          <h1>Compare PC Case</h1>
        </div>

        <div className="casingcompare-container">
          <div>
            <Row md={4}> 
              <Col md={{offset: 4}} className="casing-column-1">
                <div className="mt-4">
                  <Group>
                    <Label>PC Case 1: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit1}
                    >
                      <option selected disabled>Choose...</option>
                      {casingItems1.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {casing1.price}</h5>
                  </div>
                </div>
              </Col>
              <Col md={{ offset: 1}}>
              <div className="mt-4">
                  <Group>
                    <Label>PC Case 2: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit2}
                    >
                      <option selected disabled>Choose...</option>
                      {casingItems2.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-compare-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {casing2.price}</h5>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
            <div className="parts-compare-table">
              <table>
                <tr>
                  <th width="15%">Name</th>
                  <td width="20% "className="parts-compare-cell-1">{casing1.name}</td>
                  <td width="20%" className="parts-compare-cell-2">{casing2.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-compare-cell-1">{casing1.brand}</td>
                  <td className="parts-compare-cell-2">{casing2.brand}</td>
                </tr>
                <tr>
                  <th>Model</th>
                    <td className="parts-compare-cell-1">{casing1.model}</td>
                  <td className="parts-compare-cell-2">{casing2.model}</td>
                </tr>
                <tr>
                  <th>Tower Form Factor</th>
                  <td className="parts-compare-cell-1">{casing1.formFactor}</td>
                  <td className="parts-compare-cell-2">{casing2.formFactor}</td>
                </tr>
                <tr>
                  <th>Side Panel</th>
                  <td className="parts-compare-cell-1">{casing1.sidePanel}</td>
                  <td className="parts-compare-cell-2">{casing2.sidePanel}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-compare-cell-1">{casing1.color}</td>
                  <td className="parts-compare-cell-2">{casing2.color}</td>
                </tr>
                <tr>
                  <th>Power Supply Shroud</th>
                  <td className="parts-compare-cell-1">{casing1.shroud}</td>
                  <td className="parts-compare-cell-2">{casing2.shroud}</td>
                </tr>
                <tr>
                  <th>Maximum GPU Length</th>
                  <td className="parts-compare-cell-1">{!casing1.maxGPULength? '' : `${casing1.maxGPULength} mm`}</td>
                  <td className="parts-compare-cell-2">{!casing2.maxGPULength? '' : `${casing2.maxGPULength} mm`}</td>
                </tr>
                <tr>
                  <th>Internal 2.5" Bays</th>
                  <td className="parts-compare-cell-1">{casing1.bay2}</td>
                  <td className="parts-compare-cell-2">{casing2.bay2}</td>
                </tr>
                <tr>
                  <th>Internal 3.5" Bays</th>
                  <td className="parts-compare-cell-1">{casing1.bay3}</td>
                  <td className="parts-compare-cell-2">{casing2.bay3}</td>
                </tr>
                <tr>
                  <th>Supported Motherboard Form Factor</th>
                  <td className="parts-compare-cell-1">{casing1.supportedFormFactor}</td>
                  <td className="parts-compare-cell-2">{casing2.supportedFormFactor}</td>
                </tr>
                <tr>
                  <th>Dimension (L x H x W)</th>
                  <td className="parts-compare-cell-1">{casing1.length} mm x {casing1.height} mm x {casing1.width} mm</td>
                  <td className="parts-compare-cell-2">{casing2.length} mm x {casing2.height} mm x {casing2.width} mm</td>
                </tr>
                <tr>
                  <th>Weight</th>
                  <td className="parts-compare-cell-1">{!casing1.weight ? '' : `${casing1.weight} g`}</td>
                  <td className="parts-compare-cell-2">{!casing2.weight ? '' : `${casing2.weight} g`}</td>
                </tr>
              </table> 
            </div>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default CasingComparePage;