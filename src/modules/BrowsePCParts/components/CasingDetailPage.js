import React, { Component } from 'react';
import '../../../styles/Banner.scss';
// import TEST from '../../../assets/background.jpg';
import TEST from '../../../assets/nzxt.jpg'
import '../../../styles/PCPartsDetail.scss';
import apiCaller from '../../../tools/apiUtil';

class CasingDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      casingId: props.match.params.id,
      casing: {} 
    };
  }

  componentDidMount() {
    this.getCasing();
  }

  async getCasing() {
    const res = await apiCaller.get(`/api/case/${this.state.casingId}`);
    const casing = res.data.payload;
    this.setState({ casing });
  }


  render() {
    const { casing } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>PC Case Details</h1>
        </div>

        <div className="casingparts-detail-container">
          <div className="detail-column">
            <row>
              <div className="parts-detail-preview">
                <div className="parts-preview-a">
                  <h5 className="parts-price">RM {casing.price}</h5>
                </div>
                <div className="parts-preview-b">
                  <img className="parts-image" src={TEST} />
                </div>
              </div>
            </row>
            <row>
            <div className="parts-detail-table">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <th width="35%">Name</th>
                  <td className="parts-table-name">{casing.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-table-cell">{casing.brand}</td>
                </tr>
                <tr>
                  <th>Model</th>
                  <td className="parts-table-cell">{casing.model}</td>
                </tr>
                <tr>
                  <th>Tower Form Factor</th>
                  <td className="parts-table-cell">{casing.formFactor}</td>
                </tr>
                <tr>
                  <th>Side Panel</th>
                  <td className="parts-table-cell">{casing.sidePanel}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-table-cell">{casing.color}</td>
                </tr>
                <tr>
                  <th>Power Supply Shroud</th>
                  <td className="parts-table-cell">{casing.shroud}</td>
                </tr>
                <tr>
                  <th>Maximum GPU Length</th>
                  <td className="parts-table-cell">{casing.maxGPULength} mm</td>
                </tr>
                <tr>
                  <th>Internal 2.5" Bays</th>
                  <td className="parts-table-cell">{casing.bay2}</td>
                </tr>
                <tr>
                  <th>Internal 3.5" Bays</th>
                  <td className="parts-table-cell">{casing.bay3}</td>
                </tr>
                <tr>
                  <th>Supported Motherboard Form Factor</th>
                  <td className="parts-table-cell">{casing.supportedFormFactor}</td>
                </tr>
                <tr>
                  <th>Dimension (L x H x W)</th>
                  <td className="parts-table-cell">{casing.length} mm x {casing.height} mm x {casing.width } mm</td>
                </tr>
                <tr>
                  <th>Weight</th>
                  <td className="parts-table-cell">{casing.weight} g</td>
                </tr>
              </table> 
            </div>
            </row>
          </div>
        </div>
      </div>
    )
  }
}

export default CasingDetailPage;