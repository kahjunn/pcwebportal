import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../styles/OtherCategory.scss';

class OtherCategory extends Component{ 
  constructor(props) { 
    super(props);
  }

  render() {
    return (
      <div className="other-category">
        <div className="parts-compare-container">
          <Link to="/pc/case/compare">
            <button className="parts-compare-button"><span>Compare </span></button>
          </Link>
        </div>
        <h5>Other Category: </h5>
        <ul>
          <li><Link to="/pc/cpu">CPU</Link></li>
          <li><Link to="/pc/gpu">GPU</Link></li>
          <li><Link to="/pc/motherboard">Motherboard</Link></li>
          <li><Link to="/pc/ram">RAM</Link></li>
          <li><Link to="/pc/storage">Storage</Link></li>
          <li><Link to="/pc/power-supply">Power Supply</Link></li>
          <li><Link to="/pc/cooler">CPU Cooler</Link></li>
          <li><Link to="/pc/case">PC Case</Link></li>
        </ul> 
      </div>
    )
  }
}

export default OtherCategory;