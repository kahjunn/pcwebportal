import React, { Component } from 'react'; 
import { Link } from 'react-router-dom';
import '../../../styles/Banner.scss';
import '../styles/Category.scss';
import CPU from '../../../assets/category/cpu.svg';
import GPU from '../../../assets/category/gpu.svg';
import MOBO from '../../../assets/category/motherboard.svg';
import RAM from '../../../assets/category/ram.svg';
import POWER from '../../../assets/category/power.svg';
import STORAGE from '../../../assets/category/hdd.svg';
import COOLER from '../../../assets/category/cooling.svg';
import CASE from '../../../assets/category/case.svg';


class CategoryPage extends Component { 
  constructor(props) { 
    super(props);
  }

  render() { 
    return ( 
      <div> 
        <row>
          <div className="banner">
            <h1>Browse PC Parts</h1>
          </div>
        </row>
        <row> 
          <div className="cat-container">
            <row>
              <h1>Category</h1>
            </row>
            <row>
              <Link to="/pc/cpu">
                <div className="cat-column">
                  <img src={CPU} />
                  <h1>CPU</h1>         
                </div>
              </Link>
              <Link to="/pc/gpu">
                <div className="cat-column">
                  <img src={GPU} />
                  <h1>GPU</h1>
                </div>
              </Link>
              <Link to="/pc/motherboard">
                <div className="cat-column">
                  <img src={MOBO} />
                  <h1>Motherboard</h1>
                </div>
              </Link>
              <Link to="/pc/ram">
                <div className="cat-column">
                  <img src={RAM} />
                  <h1>RAM</h1>
                </div>
              </Link>
            </row>
            <row>
              <Link to="/pc/storage">
                <div className="cat-column">
                  <img src={STORAGE} />
                  <h1>Storage</h1>
                </div>
              </Link>
              <Link to="/pc/power-supply">
                <div className="cat-column">
                  <img src={POWER} />
                  <h1>Power Supply</h1>
                </div>
              </Link>
              <Link to="/pc/cooler">
                <div className="cat-column">
                  <img src={COOLER} />
                  <h1>CPU Cooler</h1>
                </div>
              </Link>
              <Link to="/pc/case">
                <div className="cat-column">
                  <img src={CASE} />
                  <h1>PC Case</h1>
                </div>
              </Link>
            </row>
          </div>
        </row>
      </div>
    );
  }
}

export default CategoryPage;


