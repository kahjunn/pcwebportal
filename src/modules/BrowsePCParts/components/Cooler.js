import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../styles/PCPartsTable.scss';
import CoolerOther from './CoolerOther.js';
import apiCaller from '../../../tools/apiUtil';

class CPUCooler extends Component{ 
  constructor(props) { 
    super(props);
    this.state = {
      coolerItems: []
    };
  }

  componentDidMount() {
    this.getAllCooler();
  }

  async getAllCooler() {
    const res = await apiCaller.get('/api/cooler/');
    const coolerItems = res.data.payload;
    this.setState({ coolerItems });
  }

  render() { 
    const { coolerItems } = this.state;
    console.log('Cooler: ', coolerItems);
    return (
      <div>
        <div className="banner">
          <h1>CPU Cooler</h1>
        </div>
        <div className="parts-container">        
          <CoolerOther />

          <div className="parts-table">
            <table border="0" cellspacing="0" cellpadding="0">
              <thead>
                <tr>
                  <th width="3%">#</th>
                  <th width="30%">Name</th>
                  <th width="8%">Brand</th>
                  <th width="15%">Cooler Type</th>
                  <th width="10%">Fan RPM</th>
                  <th width="9%" className="price">Price</th>
                  <th width="9%">Action</th>
                </tr>
              </thead>
              {coolerItems.map((data) => 
              <tbody>
                <tr>            
                  <td>{data.id}</td>
                  <td>{data.name}</td>
                  <td>{data.brand}</td>
                  <td>{data.coolerType}</td>
                  <td>{data.minFanRPM} - {data.maxFanRPM} RPM</td>
                  <td className="price">RM {data.price}</td> 
                  <td>
                    <Link to={`cooler/details/${data.id}`}>
                      <button className="view-button"><span>View </span></button>
                    </Link>
                  </td> 
                </tr>
              </tbody>
              )}
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default CPUCooler;