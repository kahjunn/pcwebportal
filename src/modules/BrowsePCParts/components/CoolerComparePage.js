import React, { Component } from 'react';
import { Row, Col, Column, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/cooler.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/ComparePCParts.scss';

const { Group, Label, Control } = Form;

class CoolerComparePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coolerItems1: [],
      coolerItems2: [],
      cooler1: {},
      cooler2: {},
    };
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleSubmit2 = this.handleSubmit2.bind(this);
  }

  componentDidMount() {
    this.getCooler();
  }

  async getCooler() {
    const res = await apiCaller.get('/api/cooler/');
    const coolerItems1 = res.data.payload;
    const coolerItems2 = res.data.payload;
    this.setState({ coolerItems1, coolerItems2 });
    console.log('Items1: ', coolerItems1);
    console.log('Items2: ', coolerItems2);
  }

  async handleSubmit1(event) {
    try{
      console.log('Value: ', event.target.value);
      const coolerId1 = event.target.value;
      const res = await apiCaller(`api/cooler/${coolerId1}`);
      const cooler1 = res.data.payload;
      this.setState({ cooler1 });
      // console.log('cooler returned: ', cooler1);
    } catch (e) {
      console.log(e);
    }
  }

  async handleSubmit2(event) {
    try{
      console.log('Value: ', event.target.value);
      const coolerId2 = event.target.value;
      const res = await apiCaller(`api/cooler/${coolerId2}`);
      const cooler2 = res.data.payload;
      this.setState({ cooler2 });
      console.log('cooler returned 2: ', cooler2);
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { coolerItems1, coolerItems2, cooler1, cooler2 } = this.state;
    console.log('cooler to be assigned: ', cooler1);
    console.log('cooler to be assigned 2: ', cooler2);
    return (
      <div>
        <div className="banner">
          <h1>Compare CPU Cooler</h1>
        </div>

        <div className="coolercompare-container">
          <div>
            <Row md={4}> 
              <Col md={{offset: 4}} className="cooler-column-1">
                <div className="mt-4">
                  <Group>
                    <Label>CPU Cooler 1: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit1}
                    >
                      <option selected disabled>Choose...</option>
                      {coolerItems1.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {cooler1.price}</h5>
                  </div>
                </div>
              </Col>
              <Col md={{ offset: 1}}>
              <div className="mt-4">
                  <Group>
                    <Label>CPU Cooler 2: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit2}
                    >
                      <option selected disabled>Choose...</option>
                      {coolerItems2.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-compare-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {cooler2.price}</h5>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
            <div className="parts-compare-table">
              <table>
                <tr>
                  <th width="15%">Name</th>
                  <td width="20% "className="parts-compare-cell-1">{cooler1.name}</td>
                  <td width="20%" className="parts-compare-cell-2">{cooler2.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-compare-cell-1">{cooler1.brand}</td>
                  <td className="parts-compare-cell-2">{cooler2.brand}</td>
                </tr>
                <tr>
                  <th>Model</th>
                    <td className="parts-compare-cell-1">{cooler1.model}</td>
                  <td className="parts-compare-cell-2">{cooler2.model}</td>
                </tr>
                <tr>
                  <th>Fan RPM</th>
                  <td className="parts-compare-cell-1">{cooler1.minFanRPM} - {cooler1.maxFanRPM} RPM</td>
                  <td className="parts-compare-cell-2">{cooler2.minFanRPM} - {cooler2.maxFanRPM} RPM</td>
                </tr>
                <tr>
                  <th>Cooler Type</th>
                  <td className="parts-compare-cell-1">{cooler1.coolerType}</td>
                  <td className="parts-compare-cell-2">{cooler2.coolerType}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-compare-cell-1">{cooler1.color}</td>
                  <td className="parts-compare-cell-2">{cooler2.color}</td>
                </tr>
                <tr>
                  <th>Intel Supported Sockets</th>
                  <td className="parts-compare-cell-1">{cooler1.intelSocket}</td>
                  <td className="parts-compare-cell-2">{cooler2.intelSocket}</td>
                </tr>
                <tr>
                  <th>AMD Supported Sockets</th>
                  <td className="parts-compare-cell-1">{cooler1.amdSocket}</td>
                  <td className="parts-compare-cell-2">{cooler2.amdSocket}</td>
                </tr>
              </table> 
            </div>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default CoolerComparePage;