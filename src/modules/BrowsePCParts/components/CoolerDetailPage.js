import React, { Component } from 'react';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/cooler.jpg';
import '../../../styles/PCPartsDetail.scss';
import apiCaller from '../../../tools/apiUtil';
import _ from 'lodash';

class CoolerDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coolerId: props.match.params.id,
      cooler: {} 
    };
  }

  componentDidMount() {
    this.getCooler();
  }

  async getCooler() {
    const res = await apiCaller.get(`/api/cooler/${this.state.coolerId}`);
    const cooler = res.data.payload;
    this.setState({ cooler });
  }

  render() {
    const { cooler } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>CPU Cooler Details</h1>
        </div>

        <div className="coolerparts-detail-container">
          <div className="detail-column">
            <row>
              <div className="parts-detail-preview">
                <div className="parts-preview-a">
                  <h5 className="parts-price">RM {cooler.price}</h5>
                </div>
                <div className="parts-preview-b">
                  <img className="parts-image" src={TEST} />
                </div>
              </div>
            </row>

            <row>
            <div className="parts-detail-table">
              <table>
                <tr>
                  <th width="27%">Name</th>
                  <td className="parts-table-name">{cooler.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-table-cell">{cooler.brand}</td>
                </tr>
                <tr>
                  <th>Model</th>
                  <td className="parts-table-cell">{cooler.model}</td>
                </tr>
                <tr>
                  <th>Fan RPM</th>
                  <td className="parts-table-cell">{cooler.minFanRPM} RPM - {cooler.maxFanRPM} RPM</td>
                </tr>
                <tr>
                  <th>Cooler Type</th>
                  <td className="parts-table-cell">{cooler.coolerType}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-table-cell">{cooler.color}</td>
                </tr>
                <tr>
                  <th>Intel Supported Sockets</th>
                  <td className="parts-table-cell">{ _.isEmpty(cooler.intelSocket) ? "-" : cooler.intelSocket }</td>
                </tr>
                <tr>
                  <th>AMD Supported Sockets</th>
                  <td className="parts-table-cell">{ _.isEmpty(cooler.amdSocket) ? "-" : cooler.amdSocket }</td>
                </tr>
              </table> 
            </div>
            </row>
          </div>
        </div>
      </div>
    )
  }
}

export default CoolerDetailPage;