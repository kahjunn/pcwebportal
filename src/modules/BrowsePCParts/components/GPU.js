import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../styles/PCPartsTable.scss';
import GPUOther from './GPUOther.js';
import apiCaller from '../../../tools/apiUtil';

class GPU extends Component{ 
  constructor(props) { 
    super(props);
    this.state = {
      gpuItems: []
    };
  }

  componentDidMount() {
    this.getAllGPU();
  }

  async getAllGPU() {
    const res = await apiCaller('/api/gpu/');
    console.log('Response', res);
    const gpuItems = res.data.payload;
    this.setState({ gpuItems });
  }

  render() {
    const { gpuItems } = this.state; 
    return (
      <div>
        <div className="banner">
          <h1>Graphics Processing Unit (GPU)</h1>
        </div>
        <div className="parts-container">
          <GPUOther />

          <div className="parts-table">
            <table>
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th width="20%">Name</th>
                  <th width="10%" >Brand</th>
                  <th width="10%">Chipset</th>
                  <th width="10%">Memory</th>
                  <th width="10%" className="price">Price</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              {gpuItems.map((data) => 
              <tbody>
                <tr>            
                  <td width="5%">{data.id}</td>
                  <td width="20%">{data.name}</td>
                  <td width="10%">{data.brand}</td>
                  <td width="10%">{data.chipset}</td>
                  <td width="10%">{data.memory}</td>
                  <td width="10%" className="price">RM {data.price}</td> 
                  <td width="10%">
                    <Link to={`gpu/details/${data.id}`}>
                      <button className="view-button"><span>View </span></button>
                    </Link>
                  </td> 
                </tr>
              </tbody>
              )}
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default GPU;