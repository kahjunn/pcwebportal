import React, { Component } from 'react';
import { Row, Col, Column, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/asus.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/ComparePCParts.scss';

const { Group, Label, Control } = Form;

class GPUComparePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gpuItems1: [],
      gpuItems2: [],
      gpu1: {},
      gpu2: {},
    };
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleSubmit2 = this.handleSubmit2.bind(this);
  }

  componentDidMount() {
    this.getGPU();
  }

  async getGPU() {
    const res = await apiCaller.get('/api/gpu/');
    const gpuItems1 = res.data.payload;
    const gpuItems2 = res.data.payload;
    this.setState({ gpuItems1, gpuItems2 });
    console.log('Items1: ', gpuItems1);
    console.log('Items2: ', gpuItems2);
  }

  async handleSubmit1(event) {
    try{
      console.log('Value: ', event.target.value);
      const gpuId1 = event.target.value;
      const res = await apiCaller(`api/gpu/${gpuId1}`);
      const gpu1 = res.data.payload;
      this.setState({ gpu1 });
      // console.log('gpu returned: ', gpu1);
    } catch (e) {
      console.log(e);
    }
  }

  async handleSubmit2(event) {
    try{
      console.log('Value: ', event.target.value);
      const gpuId2 = event.target.value;
      const res = await apiCaller(`api/gpu/${gpuId2}`);
      const gpu2 = res.data.payload;
      this.setState({ gpu2 });
      console.log('gpu returned 2: ', gpu2);
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { gpuItems1, gpuItems2, gpu1, gpu2 } = this.state;
    console.log('gpu to be assigned: ', gpu1);
    console.log('gpu to be assigned 2: ', gpu2);
    return (
      <div>
        <div className="banner">
          <h1>Compare GPU</h1>
        </div>

        <div className="gpucompare-container">
          <div>
            <Row md={4}> 
              <Col md={{offset: 4}} className="gpu-column-1">
                <div className="mt-4">
                  <Group>
                    <Label>GPU 1: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit1}
                    >
                      <option selected disabled>Choose...</option>
                      {gpuItems1.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {gpu1.price}</h5>
                  </div>
                </div>
              </Col>
              <Col md={{ offset: 1}}>
              <div className="mt-4">
                  <Group>
                    <Label>GPU 2: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit2}
                    >
                      <option selected disabled>Choose...</option>
                      {gpuItems2.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-compare-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {gpu2.price}</h5>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
            <div className="parts-compare-table">
              <table>
                <tr>
                  <th width="15%">Name</th>
                  <td width="20% "className="parts-compare-cell-1">{gpu1.name}</td>
                  <td width="20%" className="parts-compare-cell-2">{gpu2.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-compare-cell-1">{gpu1.brand}</td>
                  <td className="parts-compare-cell-2">{gpu2.brand}</td>
                </tr>
                <tr>
                  <th>Model</th>
                  <td className="parts-compare-cell-1">{gpu1.model}</td>
                  <td className="parts-compare-cell-2">{gpu2.model}</td>
                </tr>
                <tr>
                  <th>Chipset</th>
                  <td className="parts-compare-cell-1">{gpu1.chipset}</td>
                  <td className="parts-compare-cell-2">{gpu2.chipset}</td>
                </tr>
                <tr>
                  <th>Memory</th>
                  <td className="parts-compare-cell-1">{ !gpu1.memory ? '' : `${gpu1.memory} GB`}</td>
                  <td className="parts-compare-cell-2">{ !gpu2.memory ? '' : `${gpu2.memory} GB`}</td>
                </tr>
                <tr>
                  <th>Memory Type</th>
                  <td className="parts-compare-cell-1">{gpu1.memoryType}</td>
                  <td className="parts-compare-cell-2">{gpu2.memoryType}</td>
                </tr>
                <tr>
                  <th>Architecture</th>
                  <td className="parts-compare-cell-1">{gpu1.architecture}</td>
                  <td className="parts-compare-cell-2">{gpu2.architecture}</td>
                </tr>
                <tr>
                  <th>Core Clock</th>
                  <td className="parts-compare-cell-1">{ !gpu1.coreClock ? '' : `${gpu1.coreClock} MHz`}</td>
                  <td className="parts-compare-cell-2">{ !gpu2.coreClock ? '' : `${gpu2.coreClock} MHz`}</td>
                </tr>
                <tr>
                  <th>Boost Clock</th>
                  <td className="parts-compare-cell-1">{ !gpu1.boostClock ? '' : `${gpu1.boostClock} MHz`}</td>
                  <td className="parts-compare-cell-2">{ !gpu2.boostClock ? '' : `${gpu2.boostClock} MHz`}</td>
                </tr>
                <tr>
                  <th>Interface</th>
                  <td className="parts-compare-cell-1">{gpu1.gpuInterface}</td>
                  <td className="parts-compare-cell-2">{gpu2.gpuInterface}</td>
                </tr>
                <tr>
                  <th>SLI/Crossfire Capability</th>
                  <td className="parts-compare-cell-1">{gpu1.sliCrossfire}</td>
                  <td className="parts-compare-cell-2">{gpu2.sliCrossfire}</td>
                </tr>
                <tr>
                  <th>TDP</th>
                  <td className="parts-compare-cell-1">{ !gpu1.tdp ? '' : `${gpu1.tdp} W`}</td>
                  <td className="parts-compare-cell-2">{ !gpu2.tdp ? '' : `${gpu2.tdp} W`}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-compare-cell-1">{gpu1.color}</td>
                  <td className="parts-compare-cell-2">{gpu2.color}</td>
                </tr>
                <tr>
                  <th>Expansion Slot Width</th>
                  <td className="parts-compare-cell-1">{gpu1.expansionSlotWidth}</td>
                  <td className="parts-compare-cell-2">{gpu2.expansionSlotWidth}</td>
                </tr>
                <tr>
                  <th>Display Ports</th>
                  <td className="parts-compare-cell-1">{gpu1.displayPorts}</td>
                  <td className="parts-compare-cell-2">{gpu2.displayPorts}</td>
                </tr>
                <tr>
                  <th>HDMI Ports</th>
                  <td className="parts-compare-cell-1">{gpu1.hdmiPorts}</td>
                  <td className="parts-compare-cell-2">{gpu2.hdmiPorts}</td>
                </tr>
                <tr>
                  <th>DVI Port </th>
                  <td className="parts-compare-cell-1">{gpu1.dviPorts}</td>
                  <td className="parts-compare-cell-2">{gpu2.dviPorts}</td>
                </tr>
              </table> 
            </div>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default GPUComparePage;