import React, { Component } from 'react';
import '../../../styles/Banner.scss';
import apiCaller from '../../../tools/apiUtil';
import TEST from '../../../assets/asus.jpg';
import '../../../styles/PCPartsDetail.scss';

class GPUDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gpuId: props.match.params.id,
      gpu: {}
    };
  }

  componentDidMount() {
    this.getGPU();
  }

  async getGPU() {
    const res = await apiCaller(`/api/gpu/${this.state.gpuId}`);
    const gpu = res.data.payload;
    this.setState({ gpu });
  }

  render() {
    const { gpu } = this.state;
    console.log(gpu);
    return (
      <div>
        <div className="banner">
          <h1>Graphics Processing Unit (GPU) Details</h1>
        </div>

        <div className="gpuparts-detail-container">
          <div className="detail-column">
            <row>
              <div className="parts-detail-preview">
                <div className="parts-preview-a">
                  <h5 className="parts-price">RM {gpu.price}</h5>
                </div>
                <div className="parts-preview-b">
                  <img className="parts-image" src={TEST} />
                </div>
              </div>
            </row>

            <row>
            <div className="parts-detail-table">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <th width="40%">Name</th>
                  <td className="parts-table-name">{gpu.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-table-cell">{gpu.brand}</td>
                </tr>
                <tr>
                  <th>Model</th>
                  <td className="parts-table-cell">{gpu.model}</td>
                </tr>
                <tr>
                  <th>Chipset</th>
                  <td className="parts-table-cell">{gpu.chipset}</td>
                </tr>
                <tr>
                  <th>Memory</th>
                  <td className="parts-table-cell">{gpu.memory} GB</td>
                </tr>
                <tr>
                  <th>Memory Type</th>
                  <td className="parts-table-cell">{gpu.memoryType}</td>
                </tr>
                <tr>
                  <th>Architecture</th>
                  <td className="parts-table-cell">{gpu.architecture}</td>
                </tr>
                <tr>
                  <th>Core Clock</th>
                  <td className="parts-table-cell">{gpu.coreClock} MHz</td>
                </tr>
                <tr>
                  <th>Boost Clock</th>
                  <td className="parts-table-cell">{gpu.boostClock} MHz</td>
                </tr>
                <tr>
                  <th>Interface</th>
                  <td className="parts-table-cell">{gpu.gpuInterface}</td>
                </tr>
                <tr>
                  <th>SLI/CrossFire Capability</th>
                  <td className="parts-table-cell">{gpu.sliCrossfire}</td>
                </tr>
                <tr>
                  <th>TDP</th>
                  <td className="parts-table-cell">{gpu.tdp} W</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-table-cell">{gpu.color}</td>
                </tr>
                <tr>
                  <th>Expansion Slot Width</th>
                  <td className="parts-table-cell">{gpu.expansionSlotWidth}</td>
                </tr>
                <tr>
                  <th>Display Ports</th>
                  <td className="parts-table-cell">{gpu.displayPorts}</td>
                </tr>
                <tr>
                  <th>HDMI Ports</th>
                  <td className="parts-table-cell">{gpu.hdmiPorts}</td>
                </tr>
                <tr>
                  <th>DVI Ports</th>
                  <td className="parts-table-cell">{gpu.dviPorts}</td>
                </tr>
              </table> 
            </div>
            </row>
          </div>
        </div>
      </div>
    )
  }
}

export default GPUDetailPage;