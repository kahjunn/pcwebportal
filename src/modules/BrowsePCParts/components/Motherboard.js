import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../styles/PCPartsTable.scss';
import MotherboardOther from './MotherboardOther.js';
import apiCaller from '../../../tools/apiUtil';

class MOBO extends Component{ 
  constructor(props) { 
    super(props);
    this.state = {
      moboItems: []
    };
  }

  componentDidMount() {
    this.getAllMobo();
  }

  async getAllMobo() {
    const res = await apiCaller.get('/api/mobo/');
    const moboItems = res.data.payload;
    this.setState({ moboItems });
  }

  render() { 
    const { moboItems } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>Motherboard</h1>
        </div>
        <div className="parts-container">        
          <MotherboardOther />

          <div className="parts-table">
            <table>
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th width="20%">Name</th>
                  <th width="10%">Brand</th>
                  <th width="10%">Form Factor</th>
                  <th width="10%">Supported Socket</th>
                  <th width="10%" className="price">Price</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              {moboItems.map((data) => 
              <tbody>
                <tr>            
                  <td width="5%">{data.id}</td>
                  <td width="20%">{data.name}</td>
                  <td width="10%">{data.brand}</td>
                  <td width="10%">{data.formFactor}</td>
                  <td width="15%">{data.socket}</td>
                  <td width="10%" className="price">RM {data.price}</td> 
                  <td width="10%">
                    <Link to={`motherboard/details/${data.id}`}>
                      <button className="view-button"><span>View </span></button>
                    </Link>
                  </td> 
                </tr>
              </tbody>
              )}
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default MOBO;