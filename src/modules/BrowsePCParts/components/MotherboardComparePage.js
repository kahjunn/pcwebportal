import React, { Component } from 'react';
import { Row, Col, Column, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/gigabyte.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/ComparePCParts.scss';

const { Group, Label, Control } = Form;

class MotherboardComparePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moboItems1: [],
      moboItems2: [],
      mobo1: {},
      mobo2: {},
    };
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleSubmit2 = this.handleSubmit2.bind(this);
  }

  componentDidMount() {
    this.getMobo();
  }

  async getMobo() {
    const res = await apiCaller.get('/api/mobo/');
    const moboItems1 = res.data.payload;
    const moboItems2 = res.data.payload;
    this.setState({ moboItems1, moboItems2 });
    console.log('Items1: ', moboItems1);
    console.log('Items2: ', moboItems2);
  }

  async handleSubmit1(event) {
    try{
      console.log('Value: ', event.target.value);
      const moboId1 = event.target.value;
      const res = await apiCaller(`api/mobo/${moboId1}`);
      const mobo1 = res.data.payload;
      this.setState({ mobo1 });
      // console.log('mobo returned: ', mobo1);
    } catch (e) {
      console.log(e);
    }
  }

  async handleSubmit2(event) {
    try{
      console.log('Value: ', event.target.value);
      const moboId2 = event.target.value;
      const res = await apiCaller(`api/mobo/${moboId2}`);
      const mobo2 = res.data.payload;
      this.setState({ mobo2 });
      console.log('mobo returned 2: ', mobo2);
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { moboItems1, moboItems2, mobo1, mobo2 } = this.state;
    console.log('mobo to be assigned: ', mobo1);
    console.log('mobo to be assigned 2: ', mobo2);
    return (
      <div>
        <div className="banner">
          <h1>Compare Motherboard</h1>
        </div>

        <div className="mobocompare-container">
          <div>
            <Row md={4}> 
              <Col md={{offset: 4}} className="mobo-column-1">
                <div className="mt-4">
                  <Group>
                    <Label>Motherboard 1: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit1}
                    >
                      <option selected disabled>Choose...</option>
                      {moboItems1.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {mobo1.price}</h5>
                  </div>
                </div>
              </Col>
              <Col md={{ offset: 1}}>
              <div className="mt-4">
                  <Group>
                    <Label>Motherboard 2: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit2}
                    >
                      <option selected disabled>Choose...</option>
                      {moboItems2.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-compare-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {mobo2.price}</h5>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
            <div className="parts-compare-table">
              <table>
                <tr>
                  <th width="15%">Name</th>
                  <td width="20% "className="parts-compare-cell-1">{mobo1.name}</td>
                  <td width="20%" className="parts-compare-cell-2">{mobo2.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-compare-cell-1">{mobo1.brand}</td>
                  <td className="parts-compare-cell-2">{mobo2.brand}</td>
                </tr>
                <tr>
                  <th>Form Factor</th>
                    <td className="parts-compare-cell-1">{mobo1.formFactor}</td>
                  <td className="parts-compare-cell-2">{mobo2.formFactor}</td>
                </tr>
                <tr>
                  <th>Socket</th>
                  <td className="parts-compare-cell-1">{mobo1.socket}</td>
                  <td className="parts-compare-cell-2">{mobo2.socket}</td>
                </tr>
                <tr>
                  <th>Chipset</th>
                  <td className="parts-compare-cell-1">{mobo1.chipset}</td>
                  <td className="parts-compare-cell-2">{mobo2.chipset}</td>
                </tr>
                <tr>
                  <th>Max Supported Memory</th>
                  <td className="parts-compare-cell-1">{ !mobo1.maxMemory ? '' : `${mobo1.maxMemory} GB`}</td>
                  <td className="parts-compare-cell-2">{ !mobo2.maxMemory ? '' : `${mobo2.maxMemory} GB`}</td>
                </tr>
                <tr>
                  <th>Max Memory Slot</th>
                  <td className="parts-compare-cell-1">{mobo1.maxMemorySlot}</td>
                  <td className="parts-compare-cell-2">{mobo2.maxMemorySlot}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-compare-cell-1">{mobo1.color}</td>
                  <td className="parts-compare-cell-2">{mobo2.color}</td>
                </tr>
                <tr>
                  <th>SLI/Crossfire Capability</th>
                  <td className="parts-compare-cell-1">{mobo1.sliCrossfire} </td>
                  <td className="parts-compare-cell-2">{mobo2.sliCrossfire} </td>
                </tr>
                <tr>
                  <th>PCI-E x16 Slots</th>
                  <td className="parts-compare-cell-1">{mobo1.pcie16Slot}</td>
                  <td className="parts-compare-cell-2">{mobo2.pcie16Slot}</td>
                </tr>
                <tr>
                  <th>PCI-E x8 Slots</th>
                  <td className="parts-compare-cell-1">{mobo1.pcie8Slot}</td>
                  <td className="parts-compare-cell-2">{mobo2.pcie8Slot}</td>
                </tr>
                <tr>
                  <th>M.2 Slots</th>
                  <td className="parts-compare-cell-1">{mobo1.m2Slot}</td>
                  <td className="parts-compare-cell-2">{mobo2.m2Slot}</td>
                </tr>
                <tr>
                  <th>SATA 6 Gb/s Ports</th>
                  <td className="parts-compare-cell-1">{mobo1.sataPort}</td>
                  <td className="parts-compare-cell-2">{mobo2.sataPort}</td>
                </tr>
                <tr>
                  <th>Onboard Ethernet</th>
                  <td className="parts-compare-cell-1">{mobo1.onBoardEthernet}</td>
                  <td className="parts-compare-cell-2">{mobo2.onBoardEthernet}</td>
                </tr>
              </table> 
            </div>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default MotherboardComparePage;