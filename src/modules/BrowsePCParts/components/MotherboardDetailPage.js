import React, { Component } from 'react';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/gigabyte.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/PCPartsDetail.scss';

class MotherboardDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moboId: props.match.params.id,
      mobo: {}
    };
  }

  componentDidMount() {
    this.getMobo();
  }

  async getMobo() {
    const res = await apiCaller(`/api/mobo/${this.state.moboId}`);
    const mobo = res.data.payload;
    this.setState({ mobo });
  }
  
  render() {
    const { mobo } = this.state;
    console.log('Mobo', mobo);
    return (
      <div>
        <div className="banner">
          <h1>Motherboard Details</h1>
        </div>

        <div className="moboparts-detail-container">
          <div className="detail-column">
            <row>
              <div className="parts-detail-preview">
                <div className="parts-preview-a">
                  <h5 className="parts-price">RM {mobo.price}</h5>
                </div>
                <div className="parts-preview-b">
                  <img className="parts-image" src={TEST} />
                </div>
              </div>
            </row>

            <row>
            <div className="parts-detail-table">
              <table>
                <tr>
                  <th width="28%">Name</th>
                  <td className="parts-table-name">{mobo.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-table-cell">{mobo.brand}</td>
                </tr>
                <tr>
                  <th>Form Factor</th>
                  <td className="parts-table-cell">{mobo.formFactor}</td>
                </tr>
                <tr>
                  <th>Socket</th>
                  <td className="parts-table-cell">{mobo.socket}</td>
                </tr>
                <tr>
                  <th>Chipset</th>
                  <td className="parts-table-cell">{mobo.chipset}</td>
                </tr>
                <tr>
                  <th>Max Supported Memory</th>
                  <td className="parts-table-cell">{mobo.maxMemory} GB</td>
                </tr>
                <tr>
                  <th>Max Memory Slot</th>
                  <td className="parts-table-cell">{mobo.maxMemorySlot}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-table-cell">{mobo.color}</td>
                </tr>
                <tr>
                  <th>SLI/CrossFire Capability</th>
                  <td className="parts-table-cell">{mobo.sliCrossfire}</td>
                </tr>
                <tr>
                  <th>PCI-E x16 Slots</th>
                  <td className="parts-table-cell">{mobo.pcie16Slot}</td>
                </tr>
                <tr>
                  <th>PCI-E x8 Slots</th>
                  <td className="parts-table-cell">{mobo.pcie8Slot}</td>
                </tr>
                <tr>
                  <th>M.2 Slots</th>
                  <td className="parts-table-cell">{mobo.m2Slot}</td>
                </tr>
                <tr>
                  <th>SATA 6 Gb/s Ports</th>
                  <td className="parts-table-cell">{mobo.sataPort}</td>
                </tr>
                <tr>
                  <th>Onboard Ethernet</th>
                  <td className="parts-table-cell">{mobo.onBoardEthernet}</td>
                </tr>
              </table> 
            </div>
            </row>
          </div>
        </div>
      </div>
    )
  }
}

export default MotherboardDetailPage;