import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../styles/PCPartsTable.scss';
import {cpuData} from '../../../testData/cpuData.js';
import PowerOther from './PowerOther.js';
import apiCaller from '../../../tools/apiUtil';

class Power extends Component{ 
  constructor(props) { 
    super(props);
    this.state = {
      psuItems: []
    };
  }

  componentDidMount() {
    this.getAllPSU();
  }

  async getAllPSU() {
    const res = await apiCaller.get('/api/psu/');
    const psuItems = res.data.payload;
    this.setState({ psuItems });
  }

  render() { 
    const { psuItems } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>Power Supply Unit (PSU)</h1>
        </div>
        <div className="parts-container">        
          <PowerOther />

          <div className="parts-table">
            <table>
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th width="20%">Name</th>
                  <th width="10%">Brand</th>
                  <th width="10%">Wattage</th>
                  <th width="10%">Rating</th>
                  <th width="10%" className="price">Price</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              {psuItems.map((data) => 
              <tbody>
                <tr>            
                  <td width="5%">{data.id}</td>
                  <td width="20%">{data.name}</td>
                  <td width="10%">{data.brand}</td>
                  <td width="10%">{data.wattage} W</td>
                  <td width="10%">{data.rating}</td>
                  <td width="10%" className="price">RM {data.price}</td> 
                  <td width="10%">
                    <Link to={`power-supply/details/${data.id}`}>
                      <button className="view-button"><span>View </span></button>
                    </Link>
                  </td> 
                </tr>
              </tbody>
              )}
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default Power;