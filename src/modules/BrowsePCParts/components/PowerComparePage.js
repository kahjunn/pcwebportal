import React, { Component } from 'react';
import { Row, Col, Column, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/thermaltake.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/ComparePCParts.scss';

const { Group, Label, Control } = Form;

class PowerComparePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      psuItems1: [],
      psuItems2: [],
      psu1: {},
      psu2: {},
    };
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleSubmit2 = this.handleSubmit2.bind(this);
  }

  componentDidMount() {
    this.getPSU();
  }

  async getPSU() {
    const res = await apiCaller.get('/api/psu/');
    const psuItems1 = res.data.payload;
    const psuItems2 = res.data.payload;
    this.setState({ psuItems1, psuItems2 });
    console.log('Items1: ', psuItems1);
    console.log('Items2: ', psuItems2);
  }

  async handleSubmit1(event) {
    try{
      console.log('Value: ', event.target.value);
      const psuId1 = event.target.value;
      const res = await apiCaller(`api/psu/${psuId1}`);
      const psu1 = res.data.payload;
      this.setState({ psu1 });
      // console.log('psu returned: ', psu1);
    } catch (e) {
      console.log(e);
    }
  }

  async handleSubmit2(event) {
    try{
      console.log('Value: ', event.target.value);
      const psuId2 = event.target.value;
      const res = await apiCaller(`api/psu/${psuId2}`);
      const psu2 = res.data.payload;
      this.setState({ psu2 });
      console.log('psu returned 2: ', psu2);
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { psuItems1, psuItems2, psu1, psu2 } = this.state;
    console.log('psu to be assigned: ', psu1);
    console.log('psu to be assigned 2: ', psu2);
    return (
      <div>
        <div className="banner">
          <h1>Compare Power Supply Unit (PSU)</h1>
        </div>

        <div className="powercompare-container">
          <div>
            <Row md={4}> 
              <Col md={{offset: 4}} className="psu-column-1">
                <div className="mt-4">
                  <Group>
                    <Label>PSU 1: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit1}
                    >
                      <option selected disabled>Choose...</option>
                      {psuItems1.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {psu1.price}</h5>
                  </div>
                </div>
              </Col>
              <Col md={{ offset: 1}}>
              <div className="mt-4">
                  <Group>
                    <Label>PSU 2: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit2}
                    >
                      <option selected disabled>Choose...</option>
                      {psuItems2.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-compare-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {psu2.price}</h5>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
            <div className="parts-compare-table">
              <table>
                <tr>
                  <th width="15%">Name</th>
                  <td width="20% "className="parts-compare-cell-1">{psu1.name}</td>
                  <td width="20%" className="parts-compare-cell-2">{psu2.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-compare-cell-1">{psu1.brand}</td>
                  <td className="parts-compare-cell-2">{psu2.brand}</td>
                </tr>
                <tr>
                  <th>Model</th>
                    <td className="parts-compare-cell-1">{psu1.model}</td>
                  <td className="parts-compare-cell-2">{psu2.model}</td>
                </tr>
                <tr>
                  <th>Wattage</th>
                  <td className="parts-compare-cell-1">{ !psu1.wattage ? '' : `${psu1.wattage} W`}</td>
                  <td className="parts-compare-cell-2">{ !psu2.wattage ? '' : `${psu2.wattage} W` }</td>
                </tr>
                <tr>
                  <th>Modularity</th>
                    <td className="parts-compare-cell-1">{psu1.modularity}</td>
                  <td className="parts-compare-cell-2">{psu2.modularity}</td>
                </tr>
                <tr>
                  <th>Rating</th>
                  <td className="parts-compare-cell-1">{psu1.rating}</td>
                  <td className="parts-compare-cell-2">{psu2.rating}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-compare-cell-1">{psu1.color}</td>
                  <td className="parts-compare-cell-2">{psu2.color}</td>
                </tr>
                <tr>
                  <th>SATA Connectors</th>
                  <td className="parts-compare-cell-1">{psu1.sataConnector}</td>
                  <td className="parts-compare-cell-2">{psu2.sataConnector}</td>
                </tr>
                <tr>
                  <th>PCIe 6+2-Pin Connectors</th>
                  <td className="parts-compare-cell-1">{psu1.pcieConnector}</td>
                  <td className="parts-compare-cell-2">{psu2.pcieConnector}</td>
                </tr>
                <tr>
                  <th>Molex 4-Pin Connectors</th>
                  <td className="parts-compare-cell-1">{psu1.molexConnector}</td>
                  <td className="parts-compare-cell-2">{psu2.molexConnector}</td>
                </tr>
              </table> 
            </div>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default PowerComparePage;