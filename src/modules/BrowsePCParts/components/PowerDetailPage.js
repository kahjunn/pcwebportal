import React, { Component } from 'react';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/thermaltake.jpg';
import '../../../styles/PCPartsDetail.scss';
import apiCaller from '../../../tools/apiUtil';

class PowerDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      psuId: props.match.params.id,
      psu: {} 
    };
  }

  componentDidMount() {
    this.getPSU();
  }

  async getPSU() {
    const res = await apiCaller.get(`/api/psu/${this.state.psuId}`);
    const psu = res.data.payload;
    this.setState({ psu });
  }

  render() {
    const { psu } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>Power Supply Unit (PSU) Details</h1>
        </div>

        <div className="powerparts-detail-container">
          <div className="detail-column">
            <row>
              <div className="parts-detail-preview">
                <div className="parts-preview-a">
                  <h5 className="parts-price">RM {psu.price}</h5>
                </div>
                <div className="parts-preview-b">
                  <img className="parts-image" src={TEST} />
                </div>
              </div>
            </row>

            <row>
            <div className="parts-detail-table">
              <table>
                <tr>
                  <th width="28%">Name</th>
                  <td className="parts-table-name">{psu.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-table-cell">{psu.brand}</td>
                </tr>
                <tr>
                  <th>Wattage</th>
                  <td className="parts-table-cell">{psu.wattage} W</td>
                </tr>
                <tr>
                  <th>Modularity</th>
                  <td className="parts-table-cell">{psu.modularity}</td>
                </tr>
                <tr>
                  <th>Rating</th>
                  <td className="parts-table-cell">{psu.rating}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-table-cell">{psu.color}</td>
                </tr>
                <tr>
                  <th>SATA Connectors</th>
                  <td className="parts-table-cell">{psu.sataConnector}</td>
                </tr>
                <tr>
                  <th>PCIe 6+2-Pin Connectors</th>
                  <td className="parts-table-cell">{psu.pcieConnector}</td>
                </tr>
                <tr>
                  <th>Molex 4-Pin Connectors</th>
                  <td className="parts-table-cell">{psu.molexConnector}</td>
                </tr>
              </table> 
            </div>
            </row>
          </div>
        </div>
      </div>
    )
  }
}

export default PowerDetailPage;