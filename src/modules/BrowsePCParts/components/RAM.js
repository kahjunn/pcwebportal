import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../styles/PCPartsTable.scss';
import RAMOther from './RAMOther.js';
import apiCaller from '../../../tools/apiUtil';

class RAM extends Component{ 
  constructor(props) { 
    super(props);
    this.state = {
      ramItems: []
    };
  }

  componentDidMount() {
    this.getAllRAM();
  }

  async getAllRAM() {
    const res = await apiCaller.get('/api/ram/');
    const ramItems = res.data.payload;
    this.setState({ ramItems });
  }

  render() { 
    const { ramItems } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>Memory (RAM)</h1>
        </div>
        <div className="parts-container">        
          <RAMOther />

          <div className="parts-table">
            <table border="0" cellspacing="0" cellpadding="0">
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th width="30%">Name</th>
                  <th width="8%">Brand</th>
                  <th width="10%">Speed</th>
                  <th width="12%">No of Modules</th>
                  <th width="10%" className="price">Price</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              {ramItems.map((data) => 
              <tbody>
                <tr>            
                  <td>{data.id}</td>
                  <td>{data.name}</td>
                  <td>{data.brand}</td>
                  <td>{data.memorySpeed} MHz</td>
                  <td>{data.modules}</td>
                  <td className="price">RM {data.price}</td> 
                  <td>
                    <Link to={`ram/details/${data.id}`}>
                      <button className="view-button"><span>View </span></button>
                    </Link>
                  </td> 
                </tr>
              </tbody>
              )}
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default RAM;