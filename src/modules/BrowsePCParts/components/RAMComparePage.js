import React, { Component } from 'react';
import { Row, Col, Column, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/trident.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/ComparePCParts.scss';

const { Group, Label, Control } = Form;

class RAMComparePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ramItems1: [],
      ramItems2: [],
      ram1: {},
      ram2: {},
    };
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleSubmit2 = this.handleSubmit2.bind(this);
  }

  componentDidMount() {
    this.getRam();
  }

  async getRam() {
    const res = await apiCaller.get('/api/ram/');
    const ramItems1 = res.data.payload;
    const ramItems2 = res.data.payload;
    this.setState({ ramItems1, ramItems2 });
    console.log('Items1: ', ramItems1);
    console.log('Items2: ', ramItems2);
  }

  async handleSubmit1(event) {
    try{
      console.log('Value: ', event.target.value);
      const ramId1 = event.target.value;
      const res = await apiCaller(`api/ram/${ramId1}`);
      const ram1 = res.data.payload;
      this.setState({ ram1 });
      // console.log('ram returned: ', ram1);
    } catch (e) {
      console.log(e);
    }
  }

  async handleSubmit2(event) {
    try{
      console.log('Value: ', event.target.value);
      const ramId2 = event.target.value;
      const res = await apiCaller(`api/ram/${ramId2}`);
      const ram2 = res.data.payload;
      this.setState({ ram2 });
      console.log('ram returned 2: ', ram2);
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { ramItems1, ramItems2, ram1, ram2 } = this.state;
    console.log('ram to be assigned: ', ram1);
    console.log('ram to be assigned 2: ', ram2);
    return (
      <div>
        <div className="banner">
          <h1>Compare Memory (RAM)</h1>
        </div>

        <div className="ramcompare-container">
          <div>
            <Row md={4}> 
              <Col md={{offset: 4}} className="ram-column-1">
                <div className="mt-4">
                  <Group>
                    <Label>RAM 1: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit1}
                    >
                      <option selected disabled>Choose...</option>
                      {ramItems1.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {ram1.price}</h5>
                  </div>
                </div>
              </Col>
              <Col md={{ offset: 1}}>
              <div className="mt-4">
                  <Group>
                    <Label>RAM 2: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit2}
                    >
                      <option selected disabled>Choose...</option>
                      {ramItems2.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-compare-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {ram2.price}</h5>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
            <div className="parts-compare-table">
              <table>
                <tr>
                  <th width="15%">Name</th>
                  <td width="20% "className="parts-compare-cell-1">{ram1.name}</td>
                  <td width="20%" className="parts-compare-cell-2">{ram2.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-compare-cell-1">{ram1.brand}</td>
                  <td className="parts-compare-cell-2">{ram2.brand}</td>
                </tr>
                <tr>
                  <th>Model</th>
                    <td className="parts-compare-cell-1">{ram1.model}</td>
                  <td className="parts-compare-cell-2">{ram2.model}</td>
                </tr>
                <tr>
                  <th>Memory Type</th>
                  <td className="parts-compare-cell-1">{ram1.memoryType}</td>
                  <td className="parts-compare-cell-2">{ram2.memoryType}</td>
                </tr>
                <tr>
                  <th>Memory Speed</th>
                  <td className="parts-compare-cell-1">{ !ram1.memorySpeed ? '' : `${ram1.memorySpeed} MHz`}</td>
                  <td className="parts-compare-cell-2">{ !ram2.memorySpeed ? '' : `${ram2.memorySpeed} MHz`}</td>
                </tr>
                <tr>
                  <th>Modules</th>
                  <td className="parts-compare-cell-1">{ram1.modules}</td>
                  <td className="parts-compare-cell-2">{ram2.modules}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-compare-cell-1">{ram1.color}</td>
                  <td className="parts-compare-cell-2">{ram2.color}</td>
                </tr>
                <tr>
                  <th>Heat Spreader</th>
                  <td className="parts-compare-cell-1">{ram1.heatSpreader}</td>
                  <td className="parts-compare-cell-2">{ram2.heatSpreader}</td>
                </tr>
                <tr>
                  <th>RGB</th>
                  <td className="parts-compare-cell-1">{ram1.rgb}</td>
                  <td className="parts-compare-cell-2">{ram2.rgb}</td>
                </tr>
              </table> 
            </div>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default RAMComparePage;