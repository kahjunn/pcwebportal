import React, { Component } from 'react';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/trident.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/PCPartsDetail.scss';

class RAMDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ramId: props.match.params.id,
      ram: {} 
    };
  }

  componentDidMount() {
    this.getRAM(); 
  }

  async getRAM() {
    const res = await apiCaller.get(`/api/ram/${this.state.ramId}`);
    console.log('Response: ', res);
    const ram = res.data.payload;
    this.setState({ ram });
  }

  render() {
    const { ram } = this.state;
    console.log('Ram: ', ram);
    return (
      <div>
        <div className="banner">
          <h1>RAM Details</h1>
        </div>

        <div className="ramparts-detail-container">
          <div className="detail-column">
            <row>
              <div className="parts-detail-preview">
                <div className="parts-preview-a">
                  <h5 className="parts-price">RM {ram.price}</h5>
                </div>
                <div className="parts-preview-b">
                  <img className="parts-image" src={TEST} />
                </div>
              </div>
            </row>

            <row>
            <div className="parts-detail-table">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <th width="26%">Name</th>
                  <td className="parts-table-name">{ram.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-table-cell">{ram.brand}</td>
                </tr>
                <tr>
                  <th>Memory Type</th>
                  <td className="parts-table-cell">{ram.memoryType}</td>
                </tr>
                <tr>
                  <th>Memory Speed</th>
                  <td className="parts-table-cell">{ram.memorySpeed} Mhz</td>
                </tr>
                <tr>
                  <th>Modules</th>
                  <td className="parts-table-cell">{ram.modules}</td>
                </tr>
                <tr>
                  <th>Color</th>
                  <td className="parts-table-cell">{ram.color}</td>
                </tr>
                <tr>
                  <th>Heat Spreader</th>
                  <td className="parts-table-cell">{ram.heatSpreader}</td>
                </tr>
                <tr>
                  <th>RGB</th>
                  <td className="parts-table-cell">{ram.rgb}</td>
                </tr>
              </table> 
            </div>
            </row>
          </div>
        </div>
      </div>
    )
  }
}

export default RAMDetailPage;