import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../../styles/PCPartsTable.scss';
import StorageOther from './StorageOther.js';
import apiCaller from '../../../tools/apiUtil';

class Storage extends Component{ 
  constructor(props) { 
    super(props);
    this.state = {
      storageItems: []
    };
  }

  componentDidMount() {
    this.getAllStorage();
  }

  async getAllStorage() {
    const res = await apiCaller('/api/storage');
    const storageItems = res.data.payload;
    this.setState({ storageItems });
  }

  render() { 
    const { storageItems } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>Storage</h1>
        </div>
        <div className="parts-container">        
          <StorageOther />

          <div className="parts-table">
            <table>
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th width="24%">Name</th>
                  <th width="8%">Brand</th>
                  <th width="8%">Volume</th>
                  <th width="10%">Form Factor</th>
                  <th width="10%">Storage Type</th>
                  <th width="10%" className="price">Price</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              {storageItems.map((data) => 
              <tbody>
                <tr>            
                  <td>{data.id}</td>
                  <td>{data.name}</td>
                  <td>{data.brand}</td>
                  <td>{data.volume} GB</td>
                  <td>{data.formFactor}</td>
                  <td>{data.storageType}</td>
                  <td className="price">RM {data.price}</td> 
                  <td>
                    <Link to={`storage/details/${data.id}`}>
                      <button className="view-button"><span>View </span></button>
                    </Link>
                  </td> 
                </tr>
              </tbody>
              )}
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default Storage;