import React, { Component } from 'react';
import { Row, Col, Column, Form } from 'react-bootstrap';
import { Formik } from 'formik';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/samsung.jpg';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/ComparePCParts.scss';

const { Group, Label, Control } = Form;

class StorageComparePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      storageItems1: [],
      storageItems2: [],
      storage1: {},
      storage2: {},
    };
    this.handleSubmit1 = this.handleSubmit1.bind(this);
    this.handleSubmit2 = this.handleSubmit2.bind(this);
  }

  componentDidMount() {
    this.getStorage();
  }

  async getStorage() {
    const res = await apiCaller.get('/api/storage/');
    const storageItems1 = res.data.payload;
    const storageItems2 = res.data.payload;
    this.setState({ storageItems1, storageItems2 });
    console.log('Items1: ', storageItems1);
    console.log('Items2: ', storageItems2);
  }

  async handleSubmit1(event) {
    try{
      console.log('Value: ', event.target.value);
      const storageId1 = event.target.value;
      const res = await apiCaller(`api/storage/${storageId1}`);
      const storage1 = res.data.payload;
      this.setState({ storage1 });
      // console.log('storage returned: ', storage1);
    } catch (e) {
      console.log(e);
    }
  }

  async handleSubmit2(event) {
    try{
      console.log('Value: ', event.target.value);
      const storageId2 = event.target.value;
      const res = await apiCaller(`api/storage/${storageId2}`);
      const storage2 = res.data.payload;
      this.setState({ storage2 });
      console.log('storage returned 2: ', storage2);
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { storageItems1, storageItems2, storage1, storage2 } = this.state;
    console.log('storage to be assigned: ', storage1);
    console.log('storage to be assigned 2: ', storage2);
    return (
      <div>
        <div className="banner">
          <h1>Compare Storage</h1>
        </div>

        <div className="storagecompare-container">
          <div>
            <Row md={4}> 
              <Col md={{offset: 4}} className="storage-column-1">
                <div className="mt-4">
                  <Group>
                    <Label>Storage 1: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit1}
                    >
                      <option selected disabled>Choose...</option>
                      {storageItems1.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {storage1.price}</h5>
                  </div>
                </div>
              </Col>
              <Col md={{ offset: 1}}>
              <div className="mt-4">
                  <Group>
                    <Label>Storage 2: </Label>
                    <Control
                      as="select"
                      onChange={this.handleSubmit2}
                    >
                      <option selected disabled>Choose...</option>
                      {storageItems2.map((data) => {
                        return (<option value={`${data.id}`}>{data.name}</option>);
                      })}
                    </Control>
                  </Group>  
                </div>
                <div className="parts-compare-preview">
                  <div>
                    <img className="parts-compare-image" src={TEST} />
                    <h5 className="parts-compare-price">RM {storage2.price}</h5>
                  </div>
                </div>
              </Col>
            </Row>

            <Row>
            <div className="parts-compare-table">
              <table>
                <tr>
                  <th width="15%">Name</th>
                  <td width="20% "className="parts-compare-cell-1">{storage1.name}</td>
                  <td width="20%" className="parts-compare-cell-2">{storage2.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-compare-cell-1">{storage1.brand}</td>
                  <td className="parts-compare-cell-2">{storage2.brand}</td>
                </tr>
                <tr>
                  <th>Model</th>
                    <td className="parts-compare-cell-1">{storage1.model}</td>
                  <td className="parts-compare-cell-2">{storage2.model}</td>
                </tr>
                <tr>
                  <th>Type</th>
                  <td className="parts-compare-cell-1">{storage1.storageType}</td>
                  <td className="parts-compare-cell-2">{storage2.storageType}</td>
                </tr>
                <tr>
                  <th>RPM Speed (HDD Only)</th>
                  <td className="parts-compare-cell-1">{ storage1.storageType === 'HDD' ? `${storage1.rpmSpeed} RPM` : '-' }</td>
                  <td className="parts-compare-cell-2">{ storage2.storageType === 'HDD' ? `${storage2.rpmSpeed} RPM` : '-' }</td>
                </tr>
                <tr>
                  <th>Volume</th>
                  <td className="parts-compare-cell-1">{storage1.volume}</td>
                  <td className="parts-compare-cell-2">{storage2.volume}</td>
                </tr>
                <tr>
                  <th>Form Factor</th>
                  <td className="parts-compare-cell-1">{storage1.formFactor}</td>
                  <td className="parts-compare-cell-2">{storage2.formFactor}</td>
                </tr>
                <tr>
                  <th>Interface</th>
                  <td className="parts-compare-cell-1">{storage1.storageInterface}</td>
                  <td className="parts-compare-cell-2">{storage2.storageInterface}</td>
                </tr>
                <tr>
                  <th>NVME</th>
                  <td className="parts-compare-cell-1">{storage1.nvme}</td>
                  <td className="parts-compare-cell-2">{storage2.nvme}</td>
                </tr>
              </table> 
            </div>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}

export default StorageComparePage;