import React, { Component } from 'react';
import '../../../styles/Banner.scss';
import TEST from '../../../assets/samsung.jpg';
import apiCaller from '../../../tools/apiUtil';
import _ from 'lodash';
import '../../../styles/PCPartsDetail.scss';

class StorageDetailPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      storageId: props.match.params.id,
      storage: {} 
    };
  }

  componentDidMount() {
    this.getStorage();
  }

  async getStorage() {
    const res = await apiCaller.get(`/api/storage/${this.state.storageId}`);
    const storage = res.data.payload;
    this.setState({ storage });
  }

  render() {
    const { storage } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>Storage Details</h1>
        </div>

        <div className="storageparts-detail-container">
          <div className="detail-column">
            <row>
              <div className="parts-detail-preview">
                <div className="parts-preview-a">
                  <h5 className="parts-price">RM {storage.price}</h5>
                </div>
                <div className="parts-preview-b">
                  <img className="parts-image" src={TEST} />
                </div>
              </div>
            </row>

            <row>
            <div className="parts-detail-table">
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <th width="24%">Name</th>
                  <td className="parts-table-name">{storage.name}</td>
                </tr>
                <tr>
                  <th>Brand</th>
                  <td className="parts-table-cell">{storage.brand}</td>
                </tr>
                <tr>
                  <th>Type</th>
                  <td className="parts-table-cell">{storage.storageType}</td>
                </tr>
                <tr>
                  <th>RPM Speed (HDD Only)</th>
                  <td className="parts-table-cell">{ storage.storageType === 'HDD' ? `${storage.rpmSpeed} RPM` : '-' }</td>
                </tr>
                <tr>
                  <th>Volume</th>
                  <td className="parts-table-cell">{storage.volume} GB</td>
                </tr>
                <tr>
                  <th>Form Factor</th>
                  <td className="parts-table-cell">{storage.formFactor}</td>
                </tr>
                <tr>
                  <th>Interface</th>
                  <td className="parts-table-cell">{storage.storageInterface}</td>
                </tr>
                <tr>
                  <th>NVME</th>
                  <td className="parts-table-cell">{storage.nvme}</td>
                </tr>
              </table> 
            </div>
            </row>
          </div>
        </div>
      </div>
    )
  }
}

export default StorageDetailPage;