import React, { Component } from 'react';
import { Formik } from 'formik';
import { Button, Form, Col } from 'react-bootstrap';
import { Modal } from 'antd';
import { Link } from 'react-router-dom';
import * as yup from 'yup';
import apiCaller from '../../../tools/apiUtil';
import '../../../styles/Banner.scss';
import '../styles/BuildAPCPage.scss';
import PreviewComponent from './PreviewComponent';
import QuotationPage from './QuotationPage';


const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const formSchema = yup.object().shape({
  cpuId: yup.string().required("*Selection is required"),
  gpuId: yup.string().required("*Selection is required"),
  moboId: yup.string().required("*Selection is required"),
  ramId: yup.string().required("*Selection is required"),
  storageId: yup.string().required("*Selection is required"),
  psuId: yup.string().required("*Selection is required"),
  coolerId: yup.string().required("*Selection is required"),
  casingId: yup.string().required("*Selection is required"),
});

class BuildAPCPage extends Component { 
  constructor(props) { 
    super(props);
    this.state = {
      cpu: [],
      gpu: [],
      mobo: [],
      ram: [],
      storage: [],
      psu: [],
      cooler: [],
      casing: [],
      cpuItem: {},
      gpuItem: {},
      moboItem: {},
      ramItem: {},
      storageItem: {},
      psuItem: {},
      coolerItem: {},
      casingItem: {},
      type: {},
      isSubmitted: false,
      formData: {}
    }
    this.handler = this.handler.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.getItems();
  }

  handler() {
    this.setState({ isSubmitted: false });
  }


  async getItems() {
    try {
      const res1 = await apiCaller.get('api/cpu/');
      const cpu = res1.data.payload;
      this.setState({ cpu }); 
      const res2 = await apiCaller.get('api/gpu/');
      const gpu = res2.data.payload;
      this.setState({ gpu });
      const res3 = await apiCaller.get('api/mobo/');
      const mobo = res3.data.payload;
      this.setState({ mobo });
      const res4 = await apiCaller.get('api/ram/');
      const ram = res4.data.payload;
      this.setState({ ram });
      const res5 = await apiCaller.get('api/storage/');
      const storage = res5.data.payload;
      this.setState({ storage });
      const res6 = await apiCaller.get('api/psu/');
      const psu = res6.data.payload;
      this.setState({ psu });
      const res7 = await apiCaller.get('api/cooler/');
      const cooler = res7.data.payload;
      this.setState({ cooler });
      const res8 = await apiCaller.get('api/case/');
      const casing = res8.data.payload;
      this.setState({ casing });
    } catch (error) {
      throw(error);
    }
  };

  handleSubmit = async(values) => {
    try {
      // this.setState({ formData: values })
      const formData = { ...values };
      this.setState({ formData });
      this.setState({isSubmitted: true });
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  async handleCPUChange(data) {
    try {
      const id = data;
      console.log('Data returned: ', data);
      const res = await apiCaller(`/api/cpu/${id}`);
      const cpuItem = res.data.payload;
      this.setState({ type: cpuItem.type});
      this.setState({ cpuItem });
    } catch (e) {
      console.log(e);
    }
  }

  async handleGPUChange(data) {
    try {
      const id = data;
      console.log('Data returned: ', data);
      const res = await apiCaller(`/api/gpu/${id}`);
      const gpuItem = res.data.payload;
      this.setState({ type: gpuItem.type});
      this.setState({ gpuItem });
      console.log('GPU: ', gpuItem);
    } catch (e) {
      console.log(e);
    }
  }

  async handleMOBOChange(data) {
    try {
      const id = data;
      console.log('Data returned: ', data);
      const res = await apiCaller(`/api/mobo/${id}`);
      const moboItem = res.data.payload;
      this.setState({ type: moboItem.type});
      this.setState({ moboItem });
      console.log('MOBO: ', moboItem);
    } catch (e) {
      console.log(e);
    }
  }

  async handleRAMChange(data) {
    try {
      const id = data;
      console.log('Data returned: ', data);
      const res = await apiCaller(`/api/ram/${id}`);
      const ramItem = res.data.payload;
      this.setState({ type: ramItem.type});
      this.setState({ ramItem });
      console.log('RAM: ', ramItem);
    } catch (e) {
      console.log(e);
    }
  }

  async handleStorageChange(data) {
    try {
      const id = data;
      console.log('Data returned: ', data);
      const res = await apiCaller(`/api/storage/${id}`);
      const storageItem = res.data.payload;
      this.setState({ type: storageItem.type});
      this.setState({ storageItem });
      console.log('Storage: ', storageItem);
    } catch (e) {
      console.log(e);
    }
  }

  async handlePSUChange(data) {
    try {
      const id = data;
      console.log('Data returned: ', data);
      const res = await apiCaller(`/api/psu/${id}`);
      const psuItem = res.data.payload;
      this.setState({ type: psuItem.type});
      this.setState({ psuItem });
      console.log('PSU: ', psuItem);
    } catch (e) {
      console.log(e);
    }
  }

  async handleCoolerChange(data) {
    try {
      const id = data;
      console.log('Data returned: ', data);
      const res = await apiCaller(`/api/cooler/${id}`);
      const coolerItem = res.data.payload;
      this.setState({ type: coolerItem.type});
      this.setState({ coolerItem });
      console.log('Cooler: ', coolerItem);
    } catch (e) {
      console.log(e);
    }
  }

  async handleCaseChange(data) {
    try {
      const id = data;
      console.log('Data returned: ', data);
      const res = await apiCaller(`/api/case/${id}`);
      const casingItem = res.data.payload;
      this.setState({ type: casingItem.type});
      this.setState({ casingItem });
      console.log('Case: ', casingItem);
    } catch (e) {
      console.log(e);
    }
  }

  render() { 
    const { cpu, gpu, mobo, ram, storage, psu, cooler, casing, cpuItem, gpuItem, moboItem, ramItem, storageItem, psuItem, coolerItem, casingItem, type, formData, isSubmitted } = this.state;
    console.log('Data to be submitted: ', formData)
    console.log('Submit status: ', isSubmitted)
    
    switch (isSubmitted) {
      case true: 
        return <QuotationPage action={this.handler} data={formData} /> 
        break;
      case false:
        return ( 
        <div> 
          <div className="banner">
            <h1>Build A PC</h1>
          </div>
          
          <div className="build-container">
            <div className="col-md-6">
              <div className="build-form">
                <Formik
                  initialValues={{
                    cpuId: "",
                    gpuId: "",
                    moboId: "",
                    ramId: "",
                    storageId: "",
                    psuId: "",
                    coolerId: "",
                    casingId: "",
                  }}
                  validationSchema={formSchema}
                  onSubmit={(values, {setSubmitting, resetForm}) => {
                    console.log("Form values: ", values);
                    this.handleSubmit(values);
                    setSubmitting(true);
                    // resetForm();
                    setSubmitting(false);
                  }}
                >
                  {({ touched, errors, values, handleChange, handleSubmit, props }) => (
                    <Form onSubmit={handleSubmit}>
                    {console.log(values)}
                    <Row>
                      <Group as={Col} md="11">
                        <Label>Computer Processing Unit (CPU) :</Label>
                        <Control
                          as="select"
                          name="cpuId"
                          size="md"
                          onChange={
                            (e) => {
                            this.handleCPUChange(e.target.value);
                            handleChange(e)}
                          }
                          isInvalid={!!errors.cpuId}
                        >
                          <option selected disabled>Choose...</option>
                          {cpu.map((data) => {
                            return (<option value={`${data.id}`}>{data.name}</option>);
                          })}
                        </Control>
                        {touched.cpuId && errors.cpuId ? <Feedback type="invalid">{errors.cpuId}</Feedback> : null}
                      </Group>
                    </Row>
                    <Row>
                      <Group as={Col} md="11">
                        <Label>Graphics Processing Unit (GPU) :</Label>
                        <Control
                          as="select"
                          name="gpuId"
                          size="md"
                          onChange={
                            (e) => {
                            this.handleGPUChange(e.target.value);
                            handleChange(e)}
                          }
                          isInvalid={!!errors.gpuId}
                        >
                          <option selected disabled>Choose...</option>
                          {gpu.map((data) => {
                            return (<option value={`${data.id}`}>{data.name}</option>);
                          })}
                        </Control>
                        {touched.gpuId && errors.gpuId ? <Feedback type="invalid">{errors.gpuId}</Feedback> : null}
                      </Group> 
                    </Row>
                    <Row>  
                      <Group as={Col} md="11">
                        <Label>Motherboard :</Label>
                        <Control
                          as="select"
                          name="moboId"
                          size="md"
                          onChange={
                            (e) => {
                            this.handleMOBOChange(e.target.value);
                            handleChange(e)}
                          }
                          isInvalid={!!errors.moboId}
                        >
                          <option selected disabled>Choose...</option>
                          {mobo.map((data) => {
                            return (<option value={`${data.id}`}>{data.name}</option>);
                          })}
                        </Control>
                        {touched.moboId && errors.moboId ? <Feedback type="invalid">{errors.moboId}</Feedback> : null}
                      </Group>
                    </Row>
                    <Row>   
                      <Group as={Col} md="11">
                        <Label>RAM :</Label>
                        <Control
                          as="select"
                          name="ramId"
                          size="md"
                          onChange={
                            (e) => {
                            this.handleRAMChange(e.target.value);
                            handleChange(e)}
                          }
                          isInvalid={!!errors.ramId}
                        >
                          <option selected disabled>Choose...</option>
                          {ram.map((data) => {
                            return (<option value={`${data.id}`}>{data.name}</option>);
                          })}
                        </Control>
                        {touched.ramId && errors.ramId ? <Feedback type="invalid">{errors.ramId}</Feedback> : null}
                      </Group>
                    </Row>
                    <Row>
                      <Group as={Col} md="11">
                        <Label>Storage :</Label>
                        <Control
                          as="select"
                          name="storageId"
                          size="md"
                          onChange={
                            (e) => {
                            this.handleStorageChange(e.target.value);
                            handleChange(e)}
                          }
                          isInvalid={!!errors.storageId}
                        >
                          <option selected disabled>Choose...</option>
                          {storage.map((data) => {
                            return (<option value={`${data.id}`}>{data.name}</option>);
                          })}
                        </Control>
                        {touched.storageId && errors.storageId ? <Feedback type="invalid">{errors.storageId}</Feedback> : null}
                      </Group>
                    </Row>
                    <Row>
                      <Group as={Col} md="11">
                        <Label>Power Supply Unit (PSU) :</Label>
                        <Control
                          as="select"
                          name="psuId"
                          size="md"
                          onChange={
                            (e) => {
                            this.handlePSUChange(e.target.value);
                            handleChange(e)}
                          }
                          isInvalid={!!errors.psuId}
                        >
                          <option selected disabled>Choose...</option>
                          {psu.map((data) => {
                            return (<option value={`${data.id}`}>{data.name}</option>);
                          })}
                        </Control>
                        {touched.psuId && errors.psuId ? <Feedback type="invalid">{errors.psuId}</Feedback> : null}
                      </Group>
                    </Row>
                    <Row>
                      <Group as={Col} md="11">
                        <Label>CPU Cooler :</Label>
                        <Control
                          as="select"
                          name="coolerId"
                          size="md"
                          onChange={
                            (e) => {
                            this.handleCoolerChange(e.target.value);
                            handleChange(e)}
                          }
                          isInvalid={!!errors.coolerId}
                        >
                          <option selected disabled>Choose...</option>
                          {cooler.map((data) => {
                            return (<option value={`${data.id}`}>{data.name}</option>);
                          })}
                        </Control>
                        {touched.coolerId && errors.coolerId ? <Feedback type="invalid">{errors.coolerId}</Feedback> : null}
                      </Group>
                    </Row>
                    <Row>
                      <Group as={Col} md="11">
                        <Label>PC Case :</Label>
                        <Control
                          as="select"
                          name="casingId"
                          size="md"
                          onChange={
                            (e) => {
                            this.handleCaseChange(e.target.value);
                            handleChange(e)}
                          }
                          isInvalid={!!errors.casingId}
                        >
                          <option selected disabled>Choose...</option>
                          {casing.map((data) => {
                            return (<option value={`${data.id}`}>{data.name}</option>);
                          })}
                        </Control>
                        {touched.casingId && errors.casingId ? <Feedback type="invalid">{errors.casingId}</Feedback> : null}
                      </Group>
                    </Row>
                    <Row>
                      <Col>
                        <Button variant="primary" type="submit">
                          Submit Build
                        </Button>
                      </Col>
                    </Row>
                    </Form>
                  )}
                </Formik>
              </div>
            </div>
            
            <div className="col-md-6">
              <PreviewComponent type={type} cpu={cpuItem} gpu={gpuItem} mobo={moboItem} ram={ramItem} storage={storageItem} psu={psuItem} cooler={coolerItem} casing={casingItem} />
            </div>
          </div>
        </div>
      );
      break;
      default:
        return null;
    }
  };
}

export default BuildAPCPage;


