import React, { Component } from 'react';
import '../styles/BuildAPCPage.scss';
import DUMMY2 from '../../../assets/intel.jpg';

class PreviewComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { type, cpu, gpu, mobo, ram, storage, psu, cooler, casing } = this.props;

    if (type === 'cpu') {
      return(
        <div>
        <div className="preview-image row">
          <img src={DUMMY2} />
        </div>
        <div className="preview-content">
          <h1 className="preview-name">{cpu.name}</h1>
          <h2 className="preview-price"> RM {cpu.price} </h2>
          <div className="preview-details"> 
            <h4 className="preview-label">Core Count : <span className="label-content">{cpu.coreCount} Cores</span></h4>
            <h4 className="preview-label">Clock Speed : <span className="label-content">{cpu.clockSpeed} GHz</span></h4>
            <h4 className="preview-label">Boost Clock Speed : <span className="label-content">{cpu.boostClockSpeed} GHz</span></h4>
            <h4 className="preview-label">Supported Socket : <span className="label-content">{cpu.supportedSocket}</span></h4>
            <h4 className="preview-label">Threads : <span className="label-content">{cpu.threads} Threads</span></h4>
            <h4 className="preview-label">Core-Family : <span className="label-content">{cpu.coreFamily}</span></h4>
            <h4 className="preview-label">Cache Size : <span className="label-content">{cpu.cache} MB</span></h4>
            <h4 className="preview-label">Maximum Supported Memory : <span className="label-content">{cpu.maxMemory} GB</span></h4>
            <h4 className="preview-label">Lithography : <span className="label-content">{cpu.lithography} nm</span></h4>
          </div>
        </div>
      </div>
      );
    } else if (type === 'gpu') {
      return (
        <div>
          <div className="preview-image row">
            <img src={DUMMY2} />
          </div>
          <div className="preview-content">
            <h1 className="preview-name">{gpu.name}</h1>
            <h2 className="preview-price"> RM {gpu.price}</h2>
            <div className="preview-details"> 
            <h4 className="preview-label">Chipset : <span className="label-content">{gpu.chipset}</span></h4>
            <h4 className="preview-label">Memory : <span className="label-content">{gpu.memory} GB</span></h4>
            <h4 className="preview-label">Memory Type : <span className="label-content">{gpu.memoryType}</span></h4>
            <h4 className="preview-label">Architecture : <span className="label-content">{gpu.architecture}</span></h4>
            <h4 className="preview-label">Core Clock : <span className="label-content">{gpu.coreClock} MHz</span></h4>
            <h4 className="preview-label">Boost Clock : <span className="label-content">{gpu.boostClock} MHz</span></h4>
            <h4 className="preview-label">Interface : <span className="label-content">{gpu.gpuInterface}</span></h4>
            <h4 className="preview-label">TDP : <span className="label-content">{gpu.tdp} W</span></h4>
            <h4 className="preview-label">Color : <span className="label-content">{gpu.color}</span></h4>
            <h4 className="preview-label">SLI/Crossfire : <span className="label-content">{gpu.sliCrossfire}</span></h4>
            </div>
          </div>
        </div>
      );
    } else if (type === 'mobo') {
      return (
        <div>
          <div className="preview-image row">
            <img src={DUMMY2} />
          </div>
          <div className="preview-content">
            <h1 className="preview-name">{mobo.name}</h1>
            <h2 className="preview-price"> RM {mobo.price}</h2>
            <div className="preview-details"> 
            <h4 className="preview-label">Form Factor : <span className="label-content">{mobo.formFactor}</span></h4>
            <h4 className="preview-label">Socket : <span className="label-content">{mobo.socket}</span></h4>
            <h4 className="preview-label">Chipset : <span className="label-content">{mobo.chipset}</span></h4>
            <h4 className="preview-label">Max Supported Memory : <span className="label-content">{mobo.maxMemory} GB</span></h4>
            <h4 className="preview-label">Max Memory Slot : <span className="label-content">{mobo.maxMemorySlot}</span></h4>
            <h4 className="preview-label">Color : <span className="label-content">{mobo.color}</span></h4>
            <h4 className="preview-label">PCIe x16 Slots : <span className="label-content">{mobo.pcie16Slot}</span></h4>
            <h4 className="preview-label">M.2 Slots : <span className="label-content">{mobo.m2Slot}</span></h4>
            <h4 className="preview-label">SATA 6 Gb/s Ports : <span className="label-content">{mobo.sataPort}</span></h4>
            <h4 className="preview-label">SLI/Crossfire : <span className="label-content">{mobo.sliCrossfire}</span></h4>
            </div>
          </div>
        </div>
      );
    } else if (type === 'ram') {
      return (
        <div>
          <div className="preview-image row">
            <img src={DUMMY2} />
          </div>
          <div className="preview-content">
            <h1 className="preview-name">{ram.name}</h1>
            <h2 className="preview-price"> RM {ram.price}</h2>
            <div className="preview-details">
            <h4 className="preview-label">Brand : <span className="label-content">{ram.brand}</span></h4>
            <h4 className="preview-label">Model : <span className="label-content">{ram.model}</span></h4>
            <h4 className="preview-label">Memory Type : <span className="label-content">{ram.memoryType}</span></h4>
            <h4 className="preview-label">Memory Speed : <span className="label-content">{ram.memorySpeed} MHz</span></h4>
            <h4 className="preview-label">Modules : <span className="label-content">{ram.module}</span></h4>
            <h4 className="preview-label">Color : <span className="label-content">{ram.color}</span></h4>
            <h4 className="preview-label">Heat Spreader : <span className="label-content">{ram.heatSpreader}</span></h4>
            <h4 className="preview-label">RGB : <span className="label-content">{ram.rgb}</span></h4>
            </div>
          </div>
        </div>
      );
    } else if (type === 'storage') {
      return (
        <div>
          <div className="preview-image row">
            <img src={DUMMY2} />
          </div>
          <div className="preview-content">
            <h1 className="preview-name">{storage.name}</h1>
            <h2 className="preview-price"> RM {storage.price}</h2>
            <div className="preview-details"> 
            <h4 className="preview-label">Brand : <span className="label-content">{storage.brand}</span></h4>
            <h4 className="preview-label">Model : <span className="label-content">{storage.model}</span></h4>
            <h4 className="preview-label">Type : <span className="label-content">{storage.storageType}</span></h4>
            <h4 className="preview-label">RPM Speed (HDD Only) : <span className="label-content">{ storage.storageType === 'HDD' ? `${storage.rpmSpeed} RPM` : '-' }</span></h4>
            <h4 className="preview-label">Volume : <span className="label-content">{storage.volume} GB</span></h4>
            <h4 className="preview-label">Form Factor : <span className="label-content">{storage.formFactor}</span></h4>
            <h4 className="preview-label">Interface : <span className="label-content">{storage.storageInterface}</span></h4>
            <h4 className="preview-label">NVME : <span className="label-content">{storage.nvme}</span></h4>
            </div>
          </div>
        </div>
      );
  } else if (type === 'psu') {
      return (
        <div>
          <div className="preview-image row">
            <img src={DUMMY2} />
          </div>
          <div className="preview-content">
            <h1 className="preview-name">{psu.name}</h1>
            <h2 className="preview-price"> RM {psu.price}</h2>
            <div className="preview-details"> 
            <h4 className="preview-label">Brand : <span className="label-content">{psu.brand}</span></h4>
            <h4 className="preview-label">Model : <span className="label-content">{psu.model}</span></h4>
            <h4 className="preview-label">Wattage : <span className="label-content">{psu.wattage} W</span></h4>
            <h4 className="preview-label">Modularity : <span className="label-content">{psu.modularity}</span></h4>
            <h4 className="preview-label">Rating : <span className="label-content">{psu.rating}</span></h4>
            <h4 className="preview-label">Color : <span className="label-content">{psu.color}</span></h4>
            <h4 className="preview-label">SATA Connectors : <span className="label-content">{psu.sataConnector}</span></h4>
            <h4 className="preview-label">PCIe 6+2-Pin Connectors : <span className="label-content">{psu.pcieConnector}</span></h4>
            <h4 className="preview-label">Molex 4-Pin Connectors : <span className="label-content">{psu.molexConnector}</span></h4>
            </div>
          </div>
        </div>
      );
  } else if (type === 'cooler') {
      return (
        <div>
          <div className="preview-image row"> 
            <img src={DUMMY2} />
          </div>
          <div className="preview-content">
            <h1 className="preview-name">{cooler.name}</h1>
            <h2 className="preview-price"> RM {cooler.price}</h2>
            <div className="preview-details"> 
            <h4 className="preview-label">Brand : <span className="label-content">{cooler.brand}</span></h4>
            <h4 className="preview-label">Model : <span className="label-content">{cooler.model}</span></h4>
            <h4 className="preview-label">Fan RPM : <span className="label-content">{cooler.minFanRPM} - {cooler.maxFanRPM} RPM</span></h4>
            <h4 className="preview-label">Cooler Type : <span className="label-content">{cooler.coolerType}</span></h4>
            <h4 className="preview-label">Color : <span className="label-content">{cooler.color}</span></h4>
            <h4 className="preview-label">Intel Supported Sockets : </h4>
            <span className="label-below">{cooler.intelSocket}</span>
            <h4 className="preview-label">AMD Supported Sockets : </h4>
            <span className="label-below">{cooler.amdSocket}</span>
            </div>
          </div>
        </div>
      );
  } else if (type === 'casing') {
      return (
        <div>
        <div className="preview-image row"> 
          <img src={DUMMY2} />
        </div>
        <div className="preview-content">
          <h1 className="preview-name">{casing.name}</h1>
          <h2 className="preview-price"> RM {casing.price}</h2>
          <div className="preview-details"> 
          <h4 className="preview-label">Tower Form Factor : <span className="label-content">{casing.formFactor}</span></h4>
          <h4 className="preview-label">Side Panel : <span className="label-content">{casing.sidePanel}</span></h4>
          <h4 className="preview-label">Color : <span className="label-content">{casing.color}</span></h4>
          <h4 className="preview-label">Power Supply Shroud : <span className="label-content">{casing.shroud}</span></h4>
          <h4 className="preview-label">Maximum GPU Length : <span className="label-content">{casing.maxGPULength} mm</span></h4>
          <h4 className="preview-label">Internal 2.5" Bays : <span className="label-content">{casing.bay2}</span></h4>
          <h4 className="preview-label">Internal 3.5" Bays : <span className="label-content">{casing.bay3}</span></h4>
          <h4 className="preview-label">Supported Motherboard Form Factor : </h4>
          <span className="label-below">{casing.supportedFormFactor}</span>
          <h4 className="preview-label">Dimension (L x H x W) : <span className="label-content">{casing.length} x {casing.height} x {casing.width}</span></h4>
          <h4 className="preview-label">Weight : <span className="label-content">{casing.weight}</span></h4>
          </div>
        </div>
      </div>
      );
  }else {
      return (
        <div>
        <div className="preview-image row">
          <img src={DUMMY2} />
        </div>
        <div className="preview-content">
          <h1 className="preview-name">N/A</h1>
          <h2 className="preview-price">RM </h2>
          <div className="preview-details"> 
            <p> Brand: N/A</p>
            <p> Model: N/A</p>
            <p> Attributes...</p>
          </div>
        </div>
      </div>
      );
    }
  }
}

export default PreviewComponent;