import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { Modal } from 'antd';
import Swal from 'sweetalert2';
import _ from 'lodash';
import apiCaller from '../../../tools/apiUtil';
import '../styles/QuotationPage.scss';

class QuotationPage extends Component{ 
  constructor(props) {
    super(props);
    this.state = {
      cpu: {},
      gpu: {},
      mobo: {},
      ram: {},
      storage: {},
      psu: {},
      cooler: {},
      casing: {},
      customer: {},
      total: [],
      values: props.data,
      cpuPrice: {}
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.getData()
    console.log('Data passed over: ', this.state.values);
  }

  async getData() {
    const res = await apiCaller.get(`/api/cpu/${this.state.values.cpuId}`);
    const cpu = res.data.payload;
    this.setState({ cpu });
    const res2 = await apiCaller.get(`/api/gpu/${this.state.values.gpuId}`);
    const gpu = res2.data.payload;
    this.setState({ gpu });
    const res3 = await apiCaller.get(`/api/mobo/${this.state.values.moboId}`);
    const mobo = res3.data.payload;
    this.setState({ mobo });
    const res4 = await apiCaller.get(`/api/ram/${this.state.values.ramId}`);
    const ram = res4.data.payload;
    this.setState({ ram });
    const res5 = await apiCaller.get(`/api/storage/${this.state.values.storageId}`);
    const storage = res5.data.payload;
    this.setState({ storage });
    const res6 = await apiCaller.get(`/api/psu/${this.state.values.psuId}`);
    const psu = res6.data.payload;
    this.setState({ psu });
    const res7 = await apiCaller.get(`/api/cooler/${this.state.values.coolerId}`);
    const cooler = res7.data.payload;
    this.setState({ cooler });
    const res8 = await apiCaller.get(`/api/case/${this.state.values.casingId}`);
    const casing = res8.data.payload;
    this.setState({ casing });


    const cpuPrice = parseFloat(cpu.price);
    const gpuPrice = parseFloat(gpu.price);
    const moboPrice = parseFloat(mobo.price);
    const ramPrice = parseFloat(ram.price);
    const storagePrice = parseFloat(storage.price);
    const psuPrice = parseFloat(psu.price);
    const coolerPrice = parseFloat(cooler.price);
    const casingPrice = parseFloat(casing.price);
    const total = _.toString((cpuPrice + gpuPrice + moboPrice + ramPrice + storagePrice + psuPrice + coolerPrice + casingPrice).toFixed(2));
    this.setState({ total });
  }

  handleSubmit() {
    Modal.confirm({
      title: `Confirm ? Submission of build will generate a new build order !`,
      okText: 'Confirm',
      okType: 'primary',
      cancelText: 'No',
      onOk: async() => {
        const formData = this.state.values;
        await apiCaller.post('api/buildOrder/create', formData);
        this.props.history.push('/user/orders');
        Swal.fire({
          icon: 'success',
          title: 'Submission Success',
          text: 'Please make payment so that we can progress with your build order!'
        });
      }
    });
  }


  render() { 
    const { cpu, gpu, mobo, ram, storage, psu, cooler, casing, total } = this.state;
    console.log('CPU: ', cpu);
    console.log('GPU: ', gpu);
    console.log('Mobo: ', mobo);
    console.log('RAM: ', ram);
    console.log('Storage: ', storage);
    console.log('PSU: ', psu);
    console.log('Cooler: ', cooler);
    console.log('Casing: ', casing);
    console.log('Total: ', total);
    return (
      <div>
        <div className="banner">
          <h1>Build A PC</h1>
        </div>

        <div className="quotation-container">
          <h1 className="quotation-header">PC Build Quotation</h1>
          <div className="quotation-backButton">
            <Button onClick={this.props.action}>
              Back & Cancel  
            </Button>
          </div>
          <div className="quotation">
            <table border="0" cellspacing="0" cellpadding="0">
              <thead>
                <tr>
                    <th>#</th>
                    <th className="text-left">PC PARTS SELECTED </th>
                    <th className="text-left" width="15%">PRICE</th>
                    <th className="text-right" width="10%">QUANTITY</th>
                    <th className="text-right" width="15%">TOTAL</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <td className="no">01</td>
                    <td className="text-left">
                      <h5 className="quotation-type">CPU</h5>
                        <h4 className="quotation-pcparts">{cpu.name}</h4>
                    </td>
                    <td className="unit">RM {cpu.price}</td>
                    <td className="qty">1</td>
                    <td className="total">RM {cpu.price}</td>
                </tr>
                <tr>
                    <td className="no">02</td>
                    <td className="text-left">
                      <h5 className="quotation-type">CPU Cooler</h5>
                        <h4 className="quotation-pcparts">{cooler.name}</h4>
                    </td>
                    <td className="unit">RM {cooler.price}</td>
                    <td className="qty">1</td>
                    <td className="total">RM {cooler.price}</td>
                </tr>
                <tr>
                    <td className="no">03</td>
                    <td className="text-left">
                      <h5 className="quotation-type">Motherboard</h5>
                        <h4 className="quotation-pcparts">{mobo.name}</h4>
                    </td>
                    <td className="unit">RM {mobo.price}</td>
                    <td className="qty">1</td>
                    <td className="total">RM {mobo.price}</td>
                </tr>
                <tr>
                    <td className="no">04</td>
                    <td className="text-left">
                      <h5 className="quotation-type">Power Supply Unit</h5>
                        <h4 className="quotation-pcparts">{psu.name}</h4>
                    </td>
                    <td className="unit">RM {psu.price}</td>
                    <td className="qty">1</td>
                    <td className="total">RM {psu.price}</td>
                </tr>
                <tr>
                    <td className="no">05</td>
                    <td className="text-left">
                      <h5 className="quotation-type">Graphics Card</h5>
                        <h4 className="quotation-pcparts">{gpu.name}</h4>
                    </td>
                    <td className="unit">RM {gpu.price}</td>
                    <td className="qty">1</td>
                    <td className="total">RM {gpu.price}</td>
                </tr>
                <tr>
                    <td className="no">06</td>
                    <td className="text-left">
                      <h5 className="quotation-type">RAM</h5>
                        <h4 className="quotation-pcparts">{ram.name}</h4>
                    </td>
                    <td className="unit">RM {ram.price}</td>
                    <td className="qty">1</td>
                    <td className="total">RM {ram.price}</td>
                </tr>
                <tr>
                    <td className="no">07</td>
                    <td className="text-left">
                      <h5 className="quotation-type">PC Case</h5>
                        <h4 className="quotation-pcparts">{casing.name}</h4>
                    </td>
                    <td className="unit">RM {casing.price}</td>
                    <td className="qty">1</td>
                    <td className="total">RM {casing.price}</td>
                </tr>
                <tr>
                    <td className="no">08</td>
                    <td className="text-left">
                      <h5 className="quotation-type">Storage</h5>
                        <h4 className="quotation-pcparts">{storage.name}</h4>
                    </td>
                    <td className="unit">RM {storage.price}</td>
                    <td className="qty">1</td>
                    <td className="total">RM {storage.price}</td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2">GRAND TOTAL</td>
                    <td>RM {total} </td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div className="quotation-submit">
            <Button variant="success" onClick={this.handleSubmit} >
              Confirm & Submit PC Build
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(QuotationPage);