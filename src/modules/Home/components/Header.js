import React, { Component } from 'react'; 
import { Link } from 'react-router-dom';
import { Row, Col, Container, Nav, Navbar } from 'react-bootstrap';
import Logo from '../../../assets/logo.svg';
import User from '../../../assets/user.svg';
import './Header.scss';


class Header extends Component {
  render() {
    return ( 
      <div>
        <header>
            <div className="header">
              <div className="header-brand col-4">
                <span className="logo"><Link to="/"><img src={Logo} /></Link></span>
                <span className="brand-name"><Link to="/">PC Building 101</Link></span>    
              </div>
              <div className="auth-header col-4">
                <span className="profile-logo">
                  <img src={User} />
                  <div className="dropdown-content">
                    <div className="droplink"><Link to="/user/account">Account Details</Link></div>
                    <div className="droplink"><Link to="/user/orders">Build Orders</Link></div>
                    <div className="droplink"><Link to="/login">Log Out</Link></div>
                  </div>
                </span>
              </div>
            </div>  
        </header>
        
        <div className="navigation-bar">
          <nav>
            <ul>
              <li><Link to="/">Home</Link></li>
              <li><Link to="/buildapc">Build A PC</Link></li>
              <li><Link to="/pc/category">Browse PC Parts</Link></li>
              <li><Link to="/pcbuilds">Browse PC Builds</Link></li>
              <li><Link to="/promotion">Promotions</Link></li>
            </ul>
          </nav>
        </div>
      </div>

    );
  } 
}

export default Header;