import React, { Component } from 'react'; 
import './HomePage.scss';
import Settings from '../../../assets/settings.svg';
import Spanner from '../../../assets/spanner.svg';
import Warranty from '../../../assets/warranty.svg';
import Eye from '../../../assets/eye.svg';
import Fan from '../../../assets/fan.svg';
import Truck from '../../../assets/truck.svg';
import Calendar from '../../../assets/calendar.svg';
import Chat from '../../../assets/chat.svg';


class HomePage extends Component { 
  constructor(props) { 
    super(props);
  }

  render() { 
    return ( 
      <div className="main">
        <row>
          <div className="main-container">
            <row>
              <h1>Welcome to PC Building 101</h1>
            </row>
            <row>
              <h3>About Us</h3>
            </row>
            <row>
              <div className="info">
              <p>Welcome to PC Building 101 as we are the pioneers in the PC Building Industry in Malaysia. We are aimed to deliver you the best PC Building 
              Experience that you can have by providing you an integrated PC Builder System that you can use to efficiently build your desired PC. Other than that, 
              our websites allows you to effortlessly browse various PC Parts and PC Builds and compare them which could better help you justify your purchase according 
              to your needs. 
              </p>
              </div>
            </row>
          </div>
        </row>

        <row>
        <div className="promises-container">
          <h1>Our Promises</h1>
          <row>
            <div className="column">
              <img src={Settings} />              
              <p>
              Free PC Assembly & Installation, so you don't have to worry about doing it yourself
              </p>
            </div>
            <div className="column">             
              <img src={Spanner} />            
              <p>
              Professional Cable Management as we will definitely ensure that your cables are well organized in your PC.
              </p>
            </div>
            <div className="column">            
              <img src={Warranty} />           
              <p>
              2 Years of Warranty is covered in our PC as we build them for you, so if any issues, just hit us up at the store.
              </p>
            </div>
            <div className="column">           
              <img src={Eye} />           
              <p>
              Extensive and Professional Stress Test will be conducted on all our PC Builds in order to ensure that the PC delivered to you will be as tough as a tank. Quality will be our top priority.
              </p>
            </div>
          </row>
          <row>
            <div className="column">
              <img src={Fan} />
              <p>
              We provide lifetime FREE Dust Cleaning Services on our built PC so we will do the dirty work for you. 
              </p>
            </div>
            <div className="column">
              <img src={Truck} />
              <p>
              Free On-Site Service within entire Klang Valley. Our team of professional technicians will perform any technical analysis or repair whenever you are in the area.
              </p>
            </div>
            <div className="column">
              <img src={Calendar} />
              <p>
              3 months / 90 days 1 to 1 exchange on our PCs or any PC Parts if any defect found.
              </p>
            </div>
            <div className="column">
              <img src={Chat} />
              <p>
              Tech Support is FREE for the entire lifetime on our built PCs. So do feel free to call us when you face any technical issues.
              </p>
            </div>
          </row>
        </div>
        </row>
      </div>
    );
  }
}

export default HomePage;


