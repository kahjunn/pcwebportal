import React, { Component } from 'react'; 
import { Route, Switch, withRouter, Redirect, Link } from 'react-router-dom';
import { Nav, Navbar } from 'react-bootstrap';
import Logo from '../../../assets/logo.svg';

class NavigationBar extends Component { 
  constructor(props) { 
    super(props);
  }

  render() { 
    return ( 
      <div className="header-bar">
        <Navbar bg="primary" variant="dark" expand="lg">
          <Navbar.Brand> 
            <Link to="/">
              <img src={Logo} /> PC Building 101
            </Link>
          </Navbar.Brand>
        </Navbar>
      </div>
    )
  }
}

export default NavigationBar;
