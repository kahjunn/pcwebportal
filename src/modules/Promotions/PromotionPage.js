import React, { Component } from 'react';
import './PromotionPage.scss';
import PROMO from '../../assets/promo.jpg';
import apiCaller from '../../tools/apiUtil';

class PromotionPage extends Component{ 
  constructor(props) { 
    super(props);
    this.state = {
      promotion: []
    }
  }

  componentDidMount() {
    this.getPromotion();
  }

  async getPromotion() {
    const res = await apiCaller('/api/promotion/');
    const promotion = res.data.payload;
    console.log('Promo: ', promotion);
    this.setState({ promotion });
  }

  render() { 
    const { promotion } = this.state;
    return (
      <div>
        <row>
          <div className="banner">
            <h1>Promotions</h1>
          </div>
        </row>
        <row>
          <div className="promo-container">
            <div>
            {promotion.map((data) =>
              <row>
              <div>
                <img src={PROMO} />
              </div>
            </row>
             )}
            </div>
          </div>
        </row>
      </div>
    );
  }
}

export default PromotionPage;