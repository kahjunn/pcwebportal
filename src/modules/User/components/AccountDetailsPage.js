import React, { Component } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Formik, Field, ErrorMessage } from 'formik';
import apiCaller from '../../../tools/apiUtil';
import Swal from 'sweetalert2';
import * as yup from 'yup';
import { Link, withRouter } from 'react-router-dom';
import '../styles/AccountDetailsPage.scss';

const { Group, Label, Control, Row } = Form;
const { Feedback } = Form.Control;

const customerSchema = yup.object().shape({
  firstName: yup.string().required("*Field is required"),
  lastName: yup.string().required("*Field is required"),
  email: yup.string().email("*Not a valid email").required("*Email is required"),
  password: yup.string().required("*Field is required"),
  phoneNo: yup.string().required("*Field is required"),
  address1: yup.string().required("*Field is required"),
  address2: yup.string().required("*Field is required"),
  city: yup.string().required("*Field is required"),
  state: yup.string().required("*Field is required"),
  zipCode: yup.string().matches(/[0-9]/, "*Numbers only").required("*Field is required"),
});

class AccountDetailsPage extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      customerId: 1,
      customer: {}
    };
  }

  componentDidMount() {
    this.getCPU();
  }

  async getCPU() {
    const res = await apiCaller.get(`/api/customer/${this.state.customerId}`);
    const customer = res.data.payload;
    this.setState({ customer });
  }a

  handleSubmit = async(values) => {
    try {
      const formData = { ...values };
      console.log('FormData to be submitted', formData);
      await apiCaller.put(`api/customer/update/${this.state.customerId}`, formData);
      Swal.fire(
        'Success!',
        'You Account Details have been updated',
        'success'
      )
    } catch (error) {
      console.log("Failed to submit");
      throw error;
    }
  };

  render() {
    const { customer } = this.state;
    return (
      <div>
        <div className="banner">
          <h1>Account Details</h1>
        </div>

        <div className="user-container">
          <div className="user-form">
            <Formik
              initialValues={customer}
              enableReinitialize
              validationSchema={customerSchema}
              onSubmit={(values, {setSubmitting, resetForm}) => {
                console.log("Form values: ", values);
                this.handleSubmit(values);
                setSubmitting(true);
                // resetForm();
                setSubmitting(false);
              }}
            >
              {({ touched, errors, values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
                <Form onSubmit={handleSubmit}>
                {console.log(values)}
                <Row>
                  <Group as={Col} md="4" className="right-margin">
                    <Label>First Name</Label>
                    <Control
                      type="text"
                      name="firstName"
                      size="md"
                      placeholder="First Name"
                      onChange={handleChange}
                      value={values.firstName}
                      isInvalid={!!errors.firstName}
                    />
                    {touched.firstName && errors.firstName ? <Feedback type="invalid">{errors.firstName}</Feedback> : null}
                  </Group>
                  <Group as={Col} md="4">
                    <Label>Last Name</Label>
                    <Control
                      type="text"
                      name="lastName"
                      size="md"
                      placeholder="Last Name"
                      onChange={handleChange}
                      value={values.lastName}
                      isInvalid={!!errors.lastName}
                    />
                    {touched.lastName && errors.lastName ? <Feedback type="invalid">{errors.lastName}</Feedback> : null}
                  </Group>   
                </Row>
                <Row> 
                  <Group as={Col} md="4" className="right-margin">
                    <Label>Email</Label>
                    <Control
                      type="text"
                      name="email"
                      size="md"
                      placeholder="Email"
                      onChange={handleChange}
                      value={values.email}
                      isInvalid={!!errors.email}
                    />
                    {touched.email && errors.email ? <Feedback type="invalid">{errors.email}</Feedback> : null}
                  </Group>   
                  <Group as={Col} md="4">
                    <Label>Password</Label>
                    <Control
                      type="password"
                      name="password"
                      size="md"
                      placeholder="Password"
                      onChange={handleChange}
                      value={values.password}
                      isInvalid={!!errors.password}
                    />
                    {touched.password && errors.password ? <Feedback type="invalid">{errors.password}</Feedback> : null}
                  </Group>
                </Row>
                <Row>
                  <Group as={Col} md="3">
                    <Label>Phone Number</Label>
                    <Control
                      type="text"
                      name="phoneNo"
                      size="md"
                      placeholder="Phone No."
                      onChange={handleChange}
                      value={values.phoneNo}
                      isInvalid={!!errors.phoneNo}
                    />
                    {touched.phoneNo && errors.phoneNo ? <Feedback type="invalid">{errors.phoneNo}</Feedback> : null}
                  </Group>
                </Row>
                <Row>
                  <Group as={Col} md="6">
                    <Label>Address 1</Label>
                    <Control
                      type="text"
                      name="address1"
                      size="md"
                      placeholder="Eg. Jalan 1 Petaling Jaya..."
                      onChange={handleChange}
                      value={values.address1}
                      isInvalid={!!errors.address1}
                    />
                    {touched.address1 && errors.address1 ? <Feedback type="invalid">{errors.address1}</Feedback> : null}
                  </Group>
                </Row >
                <Row>
                  <Group as={Col} md="6">
                    <Label>Address 2</Label>
                    <Control
                      type="text"
                      name="address2"
                      size="md"
                      placeholder="Eg. Taman Maluri ..."
                      onChange={handleChange}
                      value={values.address2}
                      isInvalid={!!errors.address2}
                    />
                    {touched.address2 && errors.address2 ? <Feedback type="invalid">{errors.address2}</Feedback> : null}
                  </Group>
                </Row>
                <Row className="mt-2">
                  <Group as={Col} md="2" className="right-margin-small">
                    <Label>City</Label>
                    <Control
                      type="text"
                      name="city"
                      size="md"
                      placeholder="City"
                      onChange={handleChange}
                      value={values.city}
                      isInvalid={!!errors.city}
                    />
                    {touched.city && errors.city ? <Feedback type="invalid">{errors.city}</Feedback> : null}
                  </Group>
                  <Group as={Col} md="2" className="right-margin-small">
                    <Label>State</Label>
                    <Control
                      as="select"
                      name="state"
                      size="md"
                      onChange={handleChange}
                      value={values.state}
                      isInvalid={!!errors.state}
                    >
                      <option disabled>Choose...</option>
                      <option value="Kedah">Kedah</option> 
                      <option value="Perlis">Perlis</option> 
                      <option value="Penang">Penang</option> 
                      <option value="Perak">Perak</option> 
                      <option value="Selangor">Selangor</option> 
                      <option value="Malacca">Malacca</option> 
                      <option value="Negeri Sembilan">Negeri Sembilan</option> 
                      <option value="Terengganu">Terengganu</option> 
                      <option value="Kelantan">Kelantan</option> 
                      <option value="Pahang">Pahang</option> 
                      <option value="Johor">Johor</option> 
                      <option value="Sabah">Sabah</option>
                      <option value="Sarawak">Sarawak</option> 
                    </Control>
                    {touched.state && errors.state ? <Feedback type="invalid">{errors.state}</Feedback> : null}
                  </Group>
                  <Group as={Col} md="2" className="right-margin-small">
                    <Label>Zip Code</Label>
                    <Control
                      type="text"
                      name="zipCode"
                      size="md"
                      placeholder="Zip Code"
                      onChange={handleChange}
                      value={values.zipCode}
                      isInvalid={!!errors.zipCode}
                    />
                    {touched.zipCode && errors.zipCode ? <Feedback type="invalid">{errors.zipCode}</Feedback> : null}
                  </Group>
                </Row>
  
                <Row>
                  <Col>
                    <Button variant="primary" type="submit">
                      Update Details
                    </Button>
                  </Col>
                </Row>
                </Form>           
              )}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

export default AccountDetailsPage;