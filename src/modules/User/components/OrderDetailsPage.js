import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { Modal } from 'antd';
import '../../../styles/Banner.scss';
import apiCaller from '../../../tools/apiUtil';
import TEST from '../../../assets/background.jpg';
import '../styles/OrderDetailsPage.scss';

class OrderDetailsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boId: props.match.params.id,
      bo: {
        cpu: {},
        gpu: {},
        motherboard: {},
        ram: {},
        storage: {},
        psu: {},
        cooler: {},
        case: {},
        customer: {}
      },
      cancelStatus: { status: "Order Cancelled" },
      buttonDisabled: false
    };
  }

  componentDidMount() {
    this.getBO();
  }

  async getBO() {
    const res = await apiCaller.get(`/api/buildOrder/${this.state.boId}`);
    const bo = res.data.payload;
    this.setState({ bo });
  }

  cancelOrder(boId, boNo) {
    const id = boId;
    const orderNo = boNo;
    const cancelStatus = { status: "Order Cancelled"};
    console.log('Passed: ', cancelStatus);
    Modal.confirm({
      title: `Are you sure to cancel: #BO ${orderNo}. This cannot be reverted once cancelled`,
      okText: 'Yes, Cancel',
      okType: 'danger',
      cancelText: 'No',
      onOk: async() => {
        await apiCaller.put(`api/buildOrder/updateStatus/${id}`, cancelStatus);
        this.setState({ buttonDisabled: true });
        this.props.history.push('/user/orders');
      }
    });
  };



  renderStatus(status) {
    switch (status) {
      case "Pending Payment": 
        return <div className="pending-status">Pending Payment</div>;
      case "Processing":
        return <div className="processing-status">Processing</div>;
      case "Build Complete": 
        return <div className="complete-status">Build Complete</div>;
      case "Order Cancelled": 
        return <div className="cancel-status">Order Cancelled</div>;
      default:
        return <span>-</span>;
    }
  }

  render() {
    const { bo, buttonDisabled } = this.state;
    console.log('BuildOrder: ', bo);
    return (
      <div>
        <div className="banner">
          <h1>Order Details</h1>
        </div>

        <div className="order-details-container">
          <div className="detail-column">
            <div className="order-details-preview">
              <div>
                <h5 className="order-details-name">#BO {bo.orderNo}</h5>
              </div>
            </div>

            <div className="status-preview">
              <h5>{this.renderStatus(bo.status)}</h5>
            </div>
            
            <div className="order-details">
              <table border="0" cellspacing="0" cellpadding="0">
                <thead>
                  <tr>
                      <th>#</th>
                      <th className="text-left">PC PARTS SELECTED </th>
                      <th className="text-left" width="15%">PRICE / unit</th>
                      <th className="text-right" width="10%">QUANTITY</th>
                      <th className="text-right" width="18%">TOTAL</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                      <td className="no">01</td>
                      <td className="text-left">
                        <h2 className="orders-type">CPU</h2>
                          <h4 className="order-details-content">{bo.cpu.name}</h4>
                      </td>
                      <td className="unit">RM {bo.cpu.price}</td>
                      <td className="qty">1</td>
                      <td className="total">RM {bo.cpu.price}</td>
                  </tr>
                  <tr>
                      <td className="no">02</td>
                      <td className="text-left">
                        <h2 className="orders-type">CPU Cooler</h2>
                          <h4 className="order-details-content">{bo.cooler.name}</h4>
                      </td>
                      <td className="unit">RM {bo.cooler.price}</td>
                      <td className="qty">1</td>
                      <td className="total">RM {bo.cooler.price}</td>
                  </tr>
                  <tr>
                      <td className="no">03</td>
                      <td className="text-left">
                        <h2 className="orders-type">Motherboard</h2>
                          <h4 className="order-details-content">{bo.motherboard.name}</h4>
                        </td>
                      <td className="unit">RM {bo.motherboard.price}</td>
                      <td className="qty">1</td>
                      <td className="total">RM {bo.motherboard.price}</td>
                  </tr>
                  <tr>
                      <td className="no">04</td>
                      <td className="text-left">
                        <h2 className="orders-type">Power Supply Unit</h2>
                          <h4 className="order-details-content">{bo.psu.name}</h4>
                        </td>
                      <td className="unit">RM {bo.psu.price}</td>
                      <td className="qty">1</td>
                      <td className="total">RM {bo.psu.price}</td>
                  </tr>
                  <tr>
                      <td className="no">05</td>
                      <td className="text-left">
                        <h2 className="orders-type">Graphics Card</h2>
                          <h4 className="order-details-content">{bo.gpu.name}</h4>
                        </td>
                      <td className="unit">RM {bo.gpu.price}</td>
                      <td className="qty">1</td>
                      <td className="total">RM {bo.gpu.price}</td>
                  </tr>
                  <tr>
                      <td className="no">06</td>
                      <td className="text-left">
                        <h2 className="orders-type">RAM</h2>
                          <h4 className="order-details-content">{bo.ram.name}</h4>
                        </td>
                      <td className="unit">RM {bo.ram.price}</td>
                      <td className="qty">1</td>
                      <td className="total">RM {bo.ram.price}</td>
                  </tr>
                  <tr>
                      <td className="no">07</td>
                      <td className="text-left">
                        <h2 className="orders-type">PC Case</h2>
                          <h4 className="order-details-content">{bo.case.name}</h4>
                        </td>
                      <td className="unit">RM {bo.case.price}</td>
                      <td className="qty">1</td>
                      <td className="total">RM {bo.case.price}</td>
                  </tr>
                  <tr>
                      <td className="no">08</td>
                      <td className="text-left">
                        <h2 className="orders-type">Storage</h2>
                          <h4 className="order-details-content">{bo.storage.name}</h4>
                      </td>
                      <td className="unit">RM {bo.storage.price}</td>
                      <td className="qty">1</td>
                      <td className="total">RM {bo.storage.price}</td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                      <td colspan="2"></td>
                      <td colspan="2">GRAND TOTAL</td>
                      <td>RM {bo.price}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <div className="cancel-button">
            <Button disabled={buttonDisabled} size="lg" variant="danger" onClick={() => this.cancelOrder(bo.id, bo.orderNo)}>Cancel Order</Button>
          </div>
        </div>
      </div>
    )
  }
}

export default OrderDetailsPage;