import React, { Component } from 'react';
import apiCaller from '../../../tools/apiUtil';
import { Tag } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import '../styles/OrderPage.scss';
import {cpuData} from '../../../testData/cpuData.js';

class OrderPage extends Component{ 
  constructor(props) { 
    super(props);
    this.state = {
      buildOrders: []
    };
  }

  componentDidMount() {
    this.getBuildOrders();
  }

  async getBuildOrders() {
    try {
      const res = await apiCaller.get('api/buildOrder/');
      const buildOrders = res.data.payload;
      this.setState({ buildOrders });
    } catch (error) {
      console.log(error);
    }
  };

  renderStatus(status) {
    switch (status) {
      case "Pending Payment": 
        return <div className="pending-status">Pending Payment</div>;
      case "Processing":
        return <div className="processing-status">Processing</div>;
      case "Build Complete": 
        return <div className="complete-status">Build Complete</div>;
      case "Order Cancelled": 
        return <div className="cancel-status">Order Cancelled</div>;
      default:
        return <span>-</span>;
    }
  }

  render() { 
    const { buildOrders } = this.state;
    console.log('Orders: ', buildOrders);
    return (
      <div>
        <div className="banner">
          <h1>Build Orders</h1>
        </div>

        <div className="order-container">
          <div className="order-table">
            <table>
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th width="20%">Order Number</th>
                  <th width="10%">Status</th>
                  <th width="15%">Order Created At:</th>
                  <th width="10%">Price</th>
                  <th width="10%">Action</th>
                </tr>
              </thead>
              {buildOrders.map((data) => 
              <tbody>
                <tr>            
                  <td>{data.id}</td>
                  <td>#BO {data.orderNo}</td>
                  <td>
                    <div>{this.renderStatus(data.status)}</div>
                  </td>
                  <td>{moment(data.createdAt).format('LL')}</td>
                  <td>RM {data.price}</td> 
                  <td>
                    <Link to={`/user/orders/details/${data.id}`}>
                      <button className="view-order-button"><span>View </span></button>
                    </Link>
                  </td> 
                </tr>
              </tbody>
              )}
            </table>  
          </div>
        </div>
      </div>
    );
  }
}

export default OrderPage;