export const gpuData = [
    {
      "id" : "1",
      "name" : "Asus ROG RTX2080Ti",
      "brand" : "NVDIA",
      "chipset" : "RTX2080Ti",
      "memory" : "8 GB",
      "price" : "RM 2000",
    },
    {
      "id" : 2,
      "name" : "MSI Gaming X RTX2070",
      "brand" : "NVDIA",
      "chipset" : "RTX2070",
      "memory" : "5 GB",
      "price" : "RM 2000",
    }
  ]